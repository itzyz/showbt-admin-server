package com.showbt.project.base.service;

import com.showbt.cloud.common.core.util.JavaBeanUtils;
import com.showbt.project.base.model.AbstractEntity;
import com.showbt.project.base.model.PageParam;
import com.showbt.project.base.repository.BaseRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@Slf4j
public abstract class BaseServiceImpl<T extends AbstractEntity<ID>, ID extends Serializable> implements BaseService<T, ID> {

    public abstract BaseRepository<T, ID> repository();

    @Override
    public List<T> findAll() {
        return repository().findAll();
    }

    @Override
    public List<T> findAll(T t) {
        return repository().findAll(Example.of(t));
    }

    @Override
    public Page<T> findAll(PageParam pageParam, T t) {
        String sort = pageParam.getSort();
        return repository().findAll(
                Example.of(t),
                PageRequest.of(pageParam.getPage() - 1,
                        pageParam.getSize(),
                        Sort.Direction.fromString(sort.split(",")[1]),
                        sort.split(",")[0]));
    }

    @Override
    public Page<T> findAll(PageParam pageParam) {
        String sort = pageParam.getSort();
        return repository().findAll(
                PageRequest.of(
                        pageParam.getPage() - 1,
                        pageParam.getSize(),
                        Sort.Direction.fromString(sort.split(",")[1]),
                        sort.split(",")[0]));
    }

    @Override
    public T findById(ID id) {
        return repository().findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void save(T t) {
        repository().save(t);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void partialUpdates(ID id, T t) {
        T tInDB = repository().findById(id).orElseThrow(() -> new RuntimeException("id=" + id + "未找到"));
        BeanUtils.copyProperties(t, tInDB, JavaBeanUtils.getNullPropertyNames(t));
        repository().save(tInDB);
    }

    @Override
    @Transactional
    public void removeById(ID id) {
        repository().deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIds(ID[] ids) {
        List<T> list = repository().findAllById(Arrays.asList(ids));
        repository().deleteAll(list);
    }

    @Override
    public boolean exists(T t) {
        return repository().exists(Example.of(t));
    }

    @Autowired
    private EntityManager em;

    private static int BATCH_SIZE = 500;

    @Override
    @Transactional
    public void batchSave(Iterable<T> var1, int batchSize) {
        Iterator<T> iterator = var1.iterator();
        int index = 0;
        if (batchSize > 1) {
            BATCH_SIZE = batchSize;
        }
        while (iterator.hasNext()) {
            em.persist(iterator.next());
            index++;
            if (index % BATCH_SIZE == 0) {
                em.flush();
                em.clear();
            }
        }
        if (index % BATCH_SIZE != 0) {
            em.flush();
            em.clear();
        }
    }

    @Override
    @Transactional
    public void batchUpdate(Iterable<T> var1, int batchSize) {
        Iterator<T> iterator = var1.iterator();
        int index = 0;
        if (batchSize > 1) {
            BATCH_SIZE = batchSize;
        }
        while (iterator.hasNext()) {
            em.merge(iterator.next());
            index++;
            if (index % BATCH_SIZE == 0) {
                em.flush();
                em.clear();
            }
        }
        if (index % BATCH_SIZE != 0) {
            em.flush();
            em.clear();
        }
    }
}
