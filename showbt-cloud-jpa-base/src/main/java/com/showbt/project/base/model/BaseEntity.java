package com.showbt.project.base.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity<ID extends Serializable> implements AbstractEntity<ID> {
    private String createBy;
    @CreationTimestamp
    @Column(updatable = false)
    private Date createTime;
    private String updateBy;
    @UpdateTimestamp
    @Column(insertable = false)
    private Date updateTime;
    private String remark;
    @Column(columnDefinition = "int default 1")
    private Integer status = null;
}
