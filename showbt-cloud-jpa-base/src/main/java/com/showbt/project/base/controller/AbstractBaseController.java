package com.showbt.project.base.controller;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface AbstractBaseController {
    @InitBinder
    default void initBinder(WebDataBinder binder) {
        /** 将前台传的yyyy-MM-dd型字符串转化为Date类型 */
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
        /** 将""转为null */
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }
}
