package com.showbt.project.base.model;

public interface AbstractEntity<ID> {
    ID getId();
}
