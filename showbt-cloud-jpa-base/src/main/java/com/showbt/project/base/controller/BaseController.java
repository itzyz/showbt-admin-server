package com.showbt.project.base.controller;

import com.showbt.cloud.common.core.util.JavaBeanUtils;
import com.showbt.cloud.common.core.util.R;
import com.showbt.project.base.model.PageParam;
import com.showbt.project.base.service.BaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public abstract class BaseController<T, ID extends Serializable> implements AbstractBaseController {

    protected abstract BaseService<T, ID> service();

    @GetMapping
    protected R<Page<T>> page(PageParam pageParam) {
        return new R<>(service().findAll(pageParam));
    }

    @GetMapping("/search")
    protected R<Page<T>> page(PageParam pageParam, T t) {
        return new R<>(service().findAll(pageParam, t));
    }

    @GetMapping("/list")
    protected R<List<T>> list() {
        return new R<>(service().findAll());
    }

    @GetMapping(value = "/list/search")
    protected R<List<T>> list(T t) {
        if (JavaBeanUtils.getNoNullPropertyNames(t).length==0)
            return new R<>(201, new ArrayList<>(), "查询条件不存在！");
        return new R<>(service().findAll(t));
    }

    @GetMapping("/{id}")
    protected R<T> findOne(@PathVariable ID id) {
        return new R<>(service().findById(id));
    }

    @GetMapping("/exists")
    protected R<Boolean> exists(T t) {
        return new R<>(service().exists(t));
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.PUT})
    protected R<Boolean> save(@RequestBody T t) {
        try {
            service().save(t);
            return new R<>(Boolean.TRUE);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            return new R<>(Boolean.FALSE, "保存失败!");
        }
    }

    @PatchMapping("/{id}")
    protected R<Boolean> partialUpdates(@PathVariable("id") ID id, @RequestBody T t) {
        try {
            service().partialUpdates(id, t);
            return new R<>(Boolean.TRUE);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new R<>(Boolean.FALSE, "更新失败!");
        }
    }

    @DeleteMapping("/{id}")
    protected R<Boolean> remove(@PathVariable ID id) {
        try {
            service().removeById(id);
            return new R<>(Boolean.TRUE);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new R<>(Boolean.FALSE, "删除失败!");
        }
    }

    @PostMapping("/batchRemove")
    protected R<Boolean> batchRemove(ID[] ids) {
        try {
            service().removeByIds(ids);
            return new R<>(Boolean.TRUE);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new R<>(Boolean.FALSE, "删除失败!");
        }
    }
}
