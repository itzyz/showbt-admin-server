package com.showbt.project.base.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PageParam {
    private int page = 1;
    private int size = 10;
    private String sort = "id,desc";
}
