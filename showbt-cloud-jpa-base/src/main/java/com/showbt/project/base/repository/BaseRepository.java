package com.showbt.project.base.repository;

import com.showbt.project.base.model.AbstractEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface BaseRepository<T extends AbstractEntity<ID>, ID extends Serializable> extends JpaRepository<T, ID> {


}
