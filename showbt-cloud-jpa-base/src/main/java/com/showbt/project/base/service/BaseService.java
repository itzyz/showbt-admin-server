package com.showbt.project.base.service;

import com.showbt.project.base.model.PageParam;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BaseService<T, ID> {
    List<T> findAll();

    List<T> findAll(T t);

    Page<T> findAll(PageParam pageParam, T t);

    Page<T> findAll(PageParam pageParam);

    T findById(ID id);

    void save(T t);

    void partialUpdates(ID id, T t);

    boolean exists(T t);

    void removeById(ID id);

    void removeByIds(ID[] ids);

    void batchSave(Iterable<T> var1, int batchSize);

    void batchUpdate(Iterable<T> var1, int batchSize);
}
