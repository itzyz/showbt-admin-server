package com.showbt;

import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories
@EnableTransactionManagement
@SpringCloudApplication
public class ShowbtSpmsBizApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShowbtSpmsBizApplication.class, args);
    }

}
