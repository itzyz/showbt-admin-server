package com.showbt.project.project.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.Module;
import com.showbt.entity.Project;
import com.showbt.project.module.repository.ModuleRepository;
import com.showbt.project.project.repository.ProjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ProjectServiceImpl extends BaseServiceImpl<Project, String> implements ProjectService {

    private ProjectRepository projectRepository;

    private ModuleRepository moduleRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository, ModuleRepository moduleRepository) {
        this.projectRepository = projectRepository;
        this.moduleRepository = moduleRepository;
    }

    @Override
    public BaseRepository<Project, String> repository() {
        return projectRepository;
    }

    @Override
    @Transactional
    public void save(Project project) {

        settingModules(project);

        projectRepository.save(project);
    }

    private void settingModules(Project project) {
        if (project.getModules() != null && !project.getModules().isEmpty()) {
            List<Module> modules = new ArrayList<>();
            project.getModules().parallelStream().forEach(m ->
                    moduleRepository.findById(m.getId()).ifPresent(modules::add));
            project.setModules(modules);
        }
    }

    @Override
    public Boolean checkUnique(String label) {
        return !projectRepository.exists(Example.of(Project.builder().label(label).build()));
    }
}
