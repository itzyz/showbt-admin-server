package com.showbt.project.project.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.Project;

public interface ProjectRepository extends BaseRepository<Project, String> {
}
