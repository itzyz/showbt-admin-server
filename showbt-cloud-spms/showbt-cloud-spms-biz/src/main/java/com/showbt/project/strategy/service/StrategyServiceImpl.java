package com.showbt.project.strategy.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.Strategy;
import com.showbt.entity.StrategyServer;
import com.showbt.entity.StrategyServerPK;
import com.showbt.project.app.repository.AppRepository;
import com.showbt.project.server.repository.ServerRepository;
import com.showbt.project.strategy.repository.StrategyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class StrategyServiceImpl extends BaseServiceImpl<Strategy, Long> implements StrategyService {

    @Autowired
    private StrategyRepository strategyRepository;

    @Autowired
    private AppRepository appRepository;

    @Autowired
    private ServerRepository serverRepository;


    @Override
    public BaseRepository<Strategy, Long> repository() {
        return strategyRepository;
    }

    @Override
    @Transactional
    public void save(Strategy strategy) {

        settingApp(strategy);

        settingServers(strategy);

        strategyRepository.save(strategy);
    }

    /**
     * 设置策略与应用的关系
     *
     * @param strategy  策略
     */
    private void settingApp(Strategy strategy) {
        if (strategy.getApp() != null && strategy.getApp().getId() != null) {
            appRepository.findById(strategy.getApp().getId()).ifPresent(strategy::setApp);
        } else {
            strategy.setApp(null);
        }
    }

    /**
     * 设置策略与服务的关系
     *
     * @param strategy  策略
     */
    private void settingServers(Strategy strategy) {
        if (strategy.getStrategyServers() != null && strategy.getStrategyServers().size() > 0) {
            List<StrategyServer> strategyServers = new ArrayList<>();
            strategy.getStrategyServers()
                    .forEach(ss ->
                            serverRepository.findById(ss.getPk().getServer().getId())
                                    .ifPresent(server -> strategyServers.add(
                                            StrategyServer.builder()
                                                    .pk(StrategyServerPK.builder()
                                                            .strategy(strategy)
                                                            .server(server)
                                                            .build())
                                                    .orderNum(ss.getOrderNum())
                                                    .build())));
            strategy.setStrategyServers(strategyServers);
        }
    }
}
