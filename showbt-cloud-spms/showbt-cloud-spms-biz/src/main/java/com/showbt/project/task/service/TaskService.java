package com.showbt.project.task.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Task;

public interface TaskService extends BaseService<Task, String> {
}
