package com.showbt.project.strategy.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Strategy;

public interface StrategyService extends BaseService<Strategy, Long> {
}
