package com.showbt.project.task.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.Task;

public interface TaskRepository extends BaseRepository<Task, String> {
}
