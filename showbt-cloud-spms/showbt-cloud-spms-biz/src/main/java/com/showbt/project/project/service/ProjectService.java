package com.showbt.project.project.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Project;

public interface ProjectService extends BaseService<Project, String> {
    public Boolean checkUnique(String label);
}
