package com.showbt.project.task.controller;

import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Task;
import com.showbt.project.task.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/task")
public class TaskController extends BaseController<Task, String> {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public BaseService<Task, String> service() {
        return taskService;
    }


}
