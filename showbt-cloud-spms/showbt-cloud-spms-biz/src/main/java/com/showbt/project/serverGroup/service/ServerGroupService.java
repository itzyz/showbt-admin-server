package com.showbt.project.serverGroup.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.ServerGroup;

public interface ServerGroupService extends BaseService<ServerGroup, Long> {
}
