package com.showbt.project.app.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.App;

import java.util.List;

public interface AppService extends BaseService<App, Long> {
    public List<App> getRootApp();
}
