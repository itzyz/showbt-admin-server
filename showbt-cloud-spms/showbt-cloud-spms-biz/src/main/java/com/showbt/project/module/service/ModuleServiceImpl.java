package com.showbt.project.module.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.Module;
import com.showbt.entity.Server;
import com.showbt.project.module.repository.ModuleRepository;
import com.showbt.project.project.repository.ProjectRepository;
import com.showbt.project.server.repository.ServerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class ModuleServiceImpl extends BaseServiceImpl<Module, String> implements ModuleService {

    private ModuleRepository moduleRepository;

    private ServerRepository serverRepository;

    private ProjectRepository projectRepository;

    @Autowired
    public ModuleServiceImpl(ModuleRepository moduleRepository,
                             ServerRepository serverRepository,
                             ProjectRepository projectRepository) {
        this.moduleRepository = moduleRepository;
        this.serverRepository = serverRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public BaseRepository<Module, String> repository() {
        return moduleRepository;
    }

    @Override
    @Transactional
    public void save(Module module) {

        settingServers(module);

        settingProject(module);

        moduleRepository.save(module);
    }

    private void settingProject(Module module) {
        if (module.getProject() != null && module.getProject().getId() != null)
            projectRepository.findById(module.getProject().getId()).ifPresent(module::setProject);
        else
            module.setProject(null);
    }

    private void settingServers(Module module) {
        if (module.getServers() != null && !module.getServers().isEmpty()) {
            List<Server> servers = new ArrayList<>();
            module.getServers().parallelStream().forEach(server -> serverRepository.findById(server.getId()).ifPresent(servers::add));
            module.setServers(servers);
        }
    }
}
