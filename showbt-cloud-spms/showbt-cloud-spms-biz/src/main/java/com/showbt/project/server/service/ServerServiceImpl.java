package com.showbt.project.server.service;

import com.showbt.cloud.common.core.util.JavaBeanUtils;
import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.RuleDict;
import com.showbt.entity.Server;
import com.showbt.entity.ServerRule;
import com.showbt.entity.ServerRulePK;
import com.showbt.project.module.repository.ModuleRepository;
import com.showbt.project.ruleDict.repository.RuleDictRepository;
import com.showbt.project.server.repository.ServerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ServerServiceImpl extends BaseServiceImpl<Server, String> implements ServerService {

    private ServerRepository serverRepository;

    private ModuleRepository moduleRepository;

    private RuleDictRepository ruleDictRepository;

    @Autowired
    public ServerServiceImpl(ServerRepository serverRepository, ModuleRepository moduleRepository, RuleDictRepository ruleDictRepository) {
        this.serverRepository = serverRepository;
        this.moduleRepository = moduleRepository;
        this.ruleDictRepository = ruleDictRepository;
    }

    @Override
    public BaseRepository<Server, String> repository() {
        return serverRepository;
    }

    @Override
    @Transactional
    public void save(Server server) {

        //添加与模块的联系
        if (server.getModule() != null && server.getModule().getId() != null)
            server.setModule(moduleRepository.findById(server.getModule().getId()).orElse(null));
        //添加与规则的联系
        settingRuleDicts(server);

        serverRepository.save(server);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(rollbackFor = Exception.class)
    public void partialUpdates(String id, Server t) {
//        Server tInDB = repository().findById(id).orElseThrow(() -> new RuntimeException("id=" + id + "未找到"));
//        BeanUtils.copyProperties(tInDB,t, JavaBeanUtils.getNoNullPropertyNames(t));
        t.setUpdateTime(new Date());
        repository().save(t);
    }

    /**
     * 设置服务与规则的关系
     *
     * @param server    服务信息
     */
    private void settingRuleDicts(Server server) {
        if (server.getConfigs() != null && !server.getConfigs().isEmpty()) {
            List<ServerRule> srs = server.getConfigs().parallelStream().map(sr -> {
                RuleDict ruleDict = ruleDictRepository.findById(sr.getPk().getRuleDict().getId()).orElse(null);
                return ServerRule.builder()
                        .pk(ServerRulePK.builder()
                                .server(server)
                                .ruleDict(ruleDict)
                                .build())
                        .args(sr.getArgs())
                        .build();
            }).collect(Collectors.toList());

            server.setConfigs(srs);
        }
    }
}
