package com.showbt.project.serverGroup.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.ServerGroup;
import com.showbt.project.serverGroup.repository.ServerGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServerGroupServiceImpl extends BaseServiceImpl<ServerGroup, Long> implements ServerGroupService {

    private ServerGroupRepository serverGroupRepository;

    @Autowired
    public ServerGroupServiceImpl(ServerGroupRepository serverGroupRepository) {
        this.serverGroupRepository = serverGroupRepository;
    }

    @Override
    public BaseRepository<ServerGroup, Long> repository() {
        return serverGroupRepository;
    }
}
