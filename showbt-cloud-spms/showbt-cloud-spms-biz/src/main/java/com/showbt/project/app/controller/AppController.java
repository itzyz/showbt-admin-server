package com.showbt.project.app.controller;

import com.showbt.cloud.common.core.util.R;
import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.App;
import com.showbt.project.app.service.AppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/app")
public class AppController extends BaseController<App, Long> {

    @Autowired
    private AppService appService;

    @Override
    public BaseService<App, Long> service() {
        return appService;
    }

    /**
     * 获取根应用
     * @return
     */
    @GetMapping("/root")
    protected R<List<App>> getRootApp() {
        return new R<>(appService.getRootApp());
    }
}
