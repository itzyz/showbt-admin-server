package com.showbt.project.module.controller;

import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Module;
import com.showbt.project.module.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/module")
public class ModuleController extends BaseController<Module, String> {

    private ModuleService moduleService;

    @Autowired
    public ModuleController(ModuleService moduleService) {
        this.moduleService = moduleService;
    }

    @Override
    public BaseService<Module, String> service() {
        return moduleService;
    }
}
