package com.showbt.project.server.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.Server;

public interface ServerRepository extends BaseRepository<Server, String> {
}
