package com.showbt.project.ruleDict.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.RuleDict;

public interface RuleDictRepository extends BaseRepository<RuleDict, String> {
}
