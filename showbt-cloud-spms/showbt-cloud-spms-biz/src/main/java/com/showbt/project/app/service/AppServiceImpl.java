package com.showbt.project.app.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.App;
import com.showbt.entity.Server;
import com.showbt.project.app.repository.AppRepository;
import com.showbt.project.server.repository.ServerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class AppServiceImpl extends BaseServiceImpl<App, Long> implements AppService {

    private AppRepository appRepository;

    private ServerRepository serverRepository;

    @Autowired
    public AppServiceImpl(AppRepository appRepository, ServerRepository serverRepository) {
        this.appRepository = appRepository;
        this.serverRepository = serverRepository;
    }

    @Override
    public BaseRepository<App, Long> repository() {
        return appRepository;
    }

    @Override
    @Transactional
    public void save(App app) {
        if (app.getParentId()!= null && app.getParentId() > 0) {
//        if (app.getParentApp() != null && app.getParentApp().getId() > 0) {
//            appRepository.findById(app.getParentApp().getId()).ifPresent(app::setParentApp);
            appRepository.findById(app.getParentId()).ifPresent(app::setParentApp);
        } else {
            app.setParentApp(null);
        }

        if (app.getServers() != null && app.getServers().size() > 0) {
            List<Server> servers = new ArrayList<>();
            app.getServers().parallelStream()
                    .forEach(server -> serverRepository.findById(server.getId()).ifPresent(servers::add));
            app.setServers(servers);
        }

        if (app.getId() == null) {
            app.setApikey(UUID.randomUUID().toString().replace("-", ""));
        }

        appRepository.save(app);
    }

    @Override
    public List<App> getRootApp() {
        return appRepository.getRootApp();
    }
}
