package com.showbt.project.serverGroup.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.ServerGroup;

public interface ServerGroupRepository extends BaseRepository<ServerGroup, Long> {
}
