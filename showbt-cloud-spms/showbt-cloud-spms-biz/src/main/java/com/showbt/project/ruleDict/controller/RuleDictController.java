package com.showbt.project.ruleDict.controller;

import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.RuleDict;
import com.showbt.project.ruleDict.service.RuleDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rule_dict")
public class RuleDictController extends BaseController<RuleDict, String> {

    private RuleDictService ruleDictService;

    @Autowired
    public RuleDictController(RuleDictService ruleDictService) {
        this.ruleDictService = ruleDictService;
    }

    @Override
    public BaseService<RuleDict, String> service() {
        return ruleDictService;
    }
}
