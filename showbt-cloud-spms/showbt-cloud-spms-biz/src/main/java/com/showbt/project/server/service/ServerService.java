package com.showbt.project.server.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Server;

public interface ServerService extends BaseService<Server, String> {

    void partialUpdates(String id, Server t);
}
