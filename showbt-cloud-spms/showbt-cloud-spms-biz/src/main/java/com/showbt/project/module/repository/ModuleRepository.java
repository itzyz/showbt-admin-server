package com.showbt.project.module.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.Module;

public interface ModuleRepository extends BaseRepository<Module, String> {
}
