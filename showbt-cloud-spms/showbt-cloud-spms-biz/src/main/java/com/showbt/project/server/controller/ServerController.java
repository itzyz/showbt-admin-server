package com.showbt.project.server.controller;

import com.showbt.cloud.common.core.util.R;
import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Server;
import com.showbt.project.server.service.ServerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/server")
public class ServerController extends BaseController<Server, String> {

    private ServerService serverService;

    @Autowired
    public ServerController(ServerService serverService) {
        this.serverService = serverService;
    }

    @Override
    public BaseService<Server, String> service() {
        return serverService;
    }

    @Override
    @PatchMapping("/{id}")
    public R<Boolean> partialUpdates(@PathVariable("id") String id, @RequestBody Server t) {
        try {
            serverService.partialUpdates(id, t);
            return new R<>(Boolean.TRUE);
        } catch (Exception e) {
            log.error(e.getMessage());
            return new R<>(Boolean.FALSE, "更新失败!");
        }
    }
}
