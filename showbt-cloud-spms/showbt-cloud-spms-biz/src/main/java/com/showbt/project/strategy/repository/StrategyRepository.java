package com.showbt.project.strategy.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.Strategy;

public interface StrategyRepository extends BaseRepository<Strategy, Long> {
}
