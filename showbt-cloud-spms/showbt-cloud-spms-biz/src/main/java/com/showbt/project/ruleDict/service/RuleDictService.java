package com.showbt.project.ruleDict.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.RuleDict;

public interface RuleDictService extends BaseService<RuleDict, String> {
}
