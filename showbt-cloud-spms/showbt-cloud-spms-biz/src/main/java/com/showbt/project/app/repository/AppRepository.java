package com.showbt.project.app.repository;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.entity.App;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AppRepository extends BaseRepository<App, Long> {
    @Query("select a from App as a where a.parentApp.id is null")
    public List<App> getRootApp();
}
