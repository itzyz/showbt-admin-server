package com.showbt.project.ruleDict.service;

import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.RuleDict;
import com.showbt.project.ruleDict.repository.RuleDictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleDictServiceImpl extends BaseServiceImpl<RuleDict, String> implements RuleDictService {

    private RuleDictRepository ruleDictRepository;

    @Autowired
    public RuleDictServiceImpl(RuleDictRepository ruleDictRepository) {
        this.ruleDictRepository = ruleDictRepository;
    }

    @Override
    public BaseRepository<RuleDict, String> repository() {
        return ruleDictRepository;
    }
}
