package com.showbt.project.module.service;

import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Module;

public interface ModuleService extends BaseService<Module, String> {
}
