package com.showbt.project.strategy.controller;

import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.Strategy;
import com.showbt.project.strategy.service.StrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/strategy")
public class StrategyController extends BaseController<Strategy, Long> {

    private StrategyService strategyService;

    @Autowired
    public StrategyController(StrategyService strategyService) {
        this.strategyService = strategyService;
    }

    @Override
    public BaseService<Strategy, Long> service() {
        return strategyService;
    }
}
