package com.showbt.project.task.service;

import com.showbt.cloud.common.core.util.DateFormatUtils;
import com.showbt.project.base.repository.BaseRepository;
import com.showbt.project.base.service.BaseServiceImpl;
import com.showbt.entity.Strategy;
import com.showbt.entity.Task;
import com.showbt.project.app.repository.AppRepository;
import com.showbt.project.strategy.repository.StrategyRepository;
import com.showbt.project.task.repository.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class TaskServiceImpl extends BaseServiceImpl<Task, String> implements TaskService {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private AppRepository appRepository;
    @Autowired
    private StrategyRepository strategyRepository;


    @Override
    public BaseRepository<Task, String> repository() {
        return taskRepository;
    }

    @Override
    @Transactional
    public void save(Task task) {

        settingApp(task);

        settingStrateies(task);

        settingIndate(task);

        taskRepository.save(task);
    }

    /**
     * 设置任务有效期
     *
     * @param task  任务
     */
    private void settingIndate(Task task) {
        if (task.getEffectiveTime() == null) {
            task.setEffectiveTime(new Date());
        }

        if (task.getExpiryTime() == null) {
            task.setExpiryTime(DateFormatUtils.transStringToDate("2099-12-31 23:59:59"));
        }
    }

    /**
     * 设置任务与策略的关系
     *
     * @param task  任务
     */
    private void settingStrateies(Task task) {
        if (task.getStrategies() != null && task.getStrategies().size() > 0) {
            List<Strategy> strategies = new ArrayList<>();
            task.getStrategies().parallelStream()
                    .forEach(strategy -> strategyRepository.findById(strategy.getId()).ifPresent(strategies::add));
            task.setStrategies(strategies);
        }
    }

    /**
     * 设置任务与应用的关系
     *
     * @param task  任务
     */
    private void settingApp(Task task) {
        if (task.getApp() != null && task.getApp().getId() != null) {
            appRepository.findById(task.getApp().getId()).ifPresent(task::setApp);
        } else {
            task.setApp(null);
        }
    }
}
