package com.showbt.project.serverGroup.controller;

import com.showbt.project.base.controller.BaseController;
import com.showbt.project.base.service.BaseService;
import com.showbt.entity.ServerGroup;
import com.showbt.project.serverGroup.service.ServerGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/serverGroup")
public class ServerGroupController extends BaseController<ServerGroup, Long> {
    private ServerGroupService serverGroupService;

    @Autowired
    public ServerGroupController(ServerGroupService serverGroupService) {
        this.serverGroupService = serverGroupService;
    }

    @Override
    protected BaseService<ServerGroup, Long> service() {
        return serverGroupService;
    }
}
