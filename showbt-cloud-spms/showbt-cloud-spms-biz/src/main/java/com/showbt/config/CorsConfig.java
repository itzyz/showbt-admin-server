package com.showbt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/*
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**")
//                .allowedOrigins("http://localhost:8089", "http://localhost:9000")
//                .allowedMethods("*")
//                .allowedHeaders("*");
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedMethods("GET");
    }
}
*/
