package com.showbt;

import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.Task;
import com.showbt.feign.RemoteTaskService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RemoteTaskTest {

    @Qualifier("remoteTaskServiceFallbackImpl")
    @Autowired
    RemoteTaskService remoteTaskService;

    @Test
    public void test1() {
        R<Task> t = remoteTaskService.findOne("8afd8aed6ea6ed16016ea6ed568a0000");
        System.out.println(t.getData().getApp());
    }
}
