package com.showbt.feign.factory;

import com.showbt.feign.RemoteTaskService;
import com.showbt.feign.fallback.RemoteTaskServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteTaskServiceFallbackFactory implements FallbackFactory<RemoteTaskService> {
    @Override
    public RemoteTaskService create(Throwable throwable) {
        RemoteTaskServiceFallbackImpl remoteTaskServiceFallback = new RemoteTaskServiceFallbackImpl();
        remoteTaskServiceFallback.setCause(throwable);
        return remoteTaskServiceFallback;
    }
}
