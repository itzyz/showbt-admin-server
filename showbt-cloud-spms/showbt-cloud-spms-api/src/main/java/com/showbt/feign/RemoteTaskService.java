package com.showbt.feign;

import com.showbt.cloud.common.core.constant.ServiceNameConstants;
import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.Task;
import com.showbt.feign.factory.RemoteTaskServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = ServiceNameConstants.SPMS_SERVICE, fallbackFactory = RemoteTaskServiceFallbackFactory.class)
public interface RemoteTaskService {

    @GetMapping(value = "/task/{taskId}")
    R<Task> findOne(@PathVariable("taskId") String taskId);
}
