package com.showbt.feign.factory;

import com.showbt.feign.RemoteServerGroupService;
import com.showbt.feign.fallback.RemoteServerGroupServiceImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteServerGroupServiceFallbackFactory implements FallbackFactory<RemoteServerGroupService> {

    @Override
    public RemoteServerGroupService create(Throwable throwable) {
        RemoteServerGroupServiceImpl remoteServerGroupService = new RemoteServerGroupServiceImpl();
        remoteServerGroupService.setCause(throwable);
        return remoteServerGroupService;
    }
}
