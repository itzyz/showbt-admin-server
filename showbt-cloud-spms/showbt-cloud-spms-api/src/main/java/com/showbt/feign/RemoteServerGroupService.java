package com.showbt.feign;

import com.showbt.cloud.common.core.constant.ServiceNameConstants;
import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.ServerGroup;
import com.showbt.feign.factory.RemoteServerGroupServiceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = ServiceNameConstants.SPMS_SERVICE, fallbackFactory = RemoteServerGroupServiceFallbackFactory.class)
public interface RemoteServerGroupService {

    @GetMapping("/serverGroup/{id}")
    R<ServerGroup> findOne(@PathVariable("id") Long groupId);
}
