package com.showbt.feign.fallback;

import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.ServerGroup;
import com.showbt.feign.RemoteServerGroupService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Setter
@Component
public class RemoteServerGroupServiceImpl implements RemoteServerGroupService {
    private Throwable cause;

    @Override
    public R<ServerGroup> findOne(Long groupId) {
        log.error("feign 获取server分组信息失败: {}", groupId, cause);
        return null;
    }
}
