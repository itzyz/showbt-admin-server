package com.showbt.feign.fallback;

import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.Task;
import com.showbt.feign.RemoteTaskService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Setter
@Component
public class RemoteTaskServiceFallbackImpl implements RemoteTaskService {

    private Throwable cause;

    @Override
    public R<Task> findOne(String taskId) {
        log.error("feign 查询任务信息失败: {}", taskId, cause);
        return null;
    }
}
