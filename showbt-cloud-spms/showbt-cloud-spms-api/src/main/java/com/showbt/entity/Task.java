package com.showbt.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cpt_task")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task extends BaseEntity<String> {
    @Id
    @GenericGenerator(name = "jpa_uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa_uuid")
    @Column(length = 32)
    private String id;
    private String taskName;
    @ManyToOne
    @JoinColumn(name = "app_id")
    @JsonIgnoreProperties({"tasks", "strategies"})
    private App app;
    @ManyToMany
    @JoinTable(name = "cpt_task_strategy",
            joinColumns = {@JoinColumn(name = "task_id")},
            inverseJoinColumns = {@JoinColumn(name = "strategy_id")})
    private List<Strategy> strategies;
    private Date effectiveTime;
    private Date expiryTime;
    @Column(columnDefinition = "int default 1")
    @Builder.Default
    private Integer isSign = 1;

    public int getEffect() {
        long curr = System.currentTimeMillis();
        if (effectiveTime == null && expiryTime == null) return 1;
        if (effectiveTime != null && expiryTime != null) {
            long eff = effectiveTime.getTime();
            long exp = expiryTime.getTime();
            if (curr < eff) {
                return 0;//未生效
            }
            if (curr >= eff && curr <= exp) {//当前时间大于生效时间且小于过期时间
                return 1;//生效
            }
            return 2;//过期
        }

        if (effectiveTime == null && expiryTime != null) {
            if (curr <= expiryTime.getTime()) return 1;//生效
            else return 2;//过期
        } else {
            if (curr < effectiveTime.getTime()) return 0;//未生效
            else return 1;//生效
        }
    }

    /**
     * 根据任务生效时间和过期时间判断任务是否有效
     * 0未生效，1：生效，2：过期
     */
    @Transient
    private int effect;
}
