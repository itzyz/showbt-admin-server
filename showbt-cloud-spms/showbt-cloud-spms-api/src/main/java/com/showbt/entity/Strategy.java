package com.showbt.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cpt_strategy")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Strategy extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String strategyName;
    @ManyToOne
    @JoinColumn(name = "app_id")
    @JsonIgnoreProperties({"strategies", "tasks", "servers"})
    private App app;

    @OneToMany(mappedBy = "pk.strategy", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<StrategyServer> strategyServers = new ArrayList<>();

    public void setStrategyServers(List<StrategyServer> strategyServers) {
        if (strategyServers != null && strategyServers.size() > 0) {
            this.strategyServers.clear();
            for (StrategyServer ss : strategyServers) {
                ss.getPk().setStrategy(this);
            }
            this.strategyServers.addAll(strategyServers);
        } else {
            this.strategyServers = strategyServers;
        }
    }

    @Column(columnDefinition = "int default 1")
    private Integer type;
    private String label;
}
