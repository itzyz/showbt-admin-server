package com.showbt.entity;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cpt_strategy_server")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StrategyServer {
    @EmbeddedId
    private StrategyServerPK pk;
    private Integer orderNum;
    private String config;
}
