package com.showbt.entity;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "cpt_server_rule")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServerRule {
    @EmbeddedId
    private ServerRulePK pk;
    private String args;
}
