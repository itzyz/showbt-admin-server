package com.showbt.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务
 */
@Entity
@Table(name = "cpt_server",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"label", "m_id"})},
        indexes = {@Index(columnList = "label")})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Server extends BaseEntity<String> {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /** 服务名 */
    private String serverName;
    /** 虚拟地址 */
    @Column(length = 32)
    private String label;
    /** 真实地址 */
    private String realPath;
    /** 模块 */
    @ManyToOne
    @JoinColumn(name = "m_id")
    @JsonIgnoreProperties("servers")
    private Module module;

    /** 过滤器和请求匹配的规则 */
    @OneToMany(mappedBy = "pk.server", cascade = CascadeType.ALL, orphanRemoval = true)
    @Builder.Default
    private List<ServerRule> configs = new ArrayList<>();

    public void setConfigs(List<ServerRule> configs) {
        if (configs != null && configs.size()>0) {
            this.configs.clear();
            for (ServerRule sr : configs){
                sr.getPk().setServer(this);
            }
            this.configs.addAll(configs);
        }else {
            this.configs = configs;
        }
    }

    /** 请求方法 */
    private String method;
    /** 固定参数 */
    private String fixedParams;
    /** 用户需提交的请求参数的参数名 */
    private String requestParamKeys;
    /** 服务结果与标准结果的映射--标准结果:服务结果 */
    private String mappingFields;
    /** 服务的组 */
    @ManyToOne
    @JoinColumn(name = "g_id")
    private ServerGroup group;
    /** v1服务的组 */
    @ManyToOne
    @JoinColumn(name = "v1_g_id")
    private ServerGroup v1Group;
    /** 自定义参数html字段 */
    @Column(columnDefinition = "text")
    private String customParam;
    /** 请求参数转换,用于解决参数值相同但参数名不同的问题 */
    private String requestKeyMapping;
    /** 请求头 */
    private String requestHeaders;
    /** 请求类别: A 聚合接口 G 普通接口 */
    private String serverType;
    /** 格式化类 */
    private String formatterClass;


}
