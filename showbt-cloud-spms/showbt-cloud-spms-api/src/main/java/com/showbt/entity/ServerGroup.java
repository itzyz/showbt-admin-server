package com.showbt.entity;

import com.showbt.project.base.model.AbstractEntity;
import lombok.*;

import javax.persistence.*;

/**
 * 服务组
 */
@Entity
@Table(name = "cpt_server_group")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServerGroup implements AbstractEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 组名
     */
    private String groupName;
    /**
     * 去重依赖字段
     */
    private String distinctFields;
    /**
     * 标准结果的key
     */
    private String mappingKeys;
    /**
     * 标签
     */
    private String label;
}
