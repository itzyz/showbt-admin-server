package com.showbt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cpt_module", uniqueConstraints = {@UniqueConstraint(columnNames = {"label", "p_id"})})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class Module extends BaseEntity<String> {
    @Id
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    private String moduleName;
    private String label;
    private String realPath;
    private String health;
    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("module")
//    @JsonIgnore
    private List<Server> servers;
    @ManyToOne
    @JoinColumn(name = "p_id")
    @JsonIgnoreProperties("modules")
    private Project project;

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
