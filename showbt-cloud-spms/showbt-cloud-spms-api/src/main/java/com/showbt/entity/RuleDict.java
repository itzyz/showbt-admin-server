package com.showbt.entity;

import com.showbt.project.base.model.AbstractEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "cpt_rule_dict",
        indexes = {@Index(columnList = "ruleType")})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDict implements AbstractEntity<String> {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    private String ruleName;
    @Column(length = 10)
    private String ruleType;
    private String args;
}
