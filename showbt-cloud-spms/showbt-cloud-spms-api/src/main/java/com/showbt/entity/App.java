package com.showbt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cpt_app")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("app")
public class App extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String appName;
    @Column(updatable = false)
    private String apikey;
    @ManyToOne
    @JoinColumn(name = "pid")
    @JsonIgnoreProperties({"strategies", "tasks", "servers","children"})
    private App parentApp;

    @Transient
    private Long parentId = 0l;

    public Long getParentId() {
        return this.parentApp !=null?this.parentApp.getId():this.parentId;
    }

    @OneToMany(mappedBy = "parentApp")
    @JsonIgnoreProperties({"strategies", "tasks", "servers"})
    private List<App> children;

    @OneToMany(mappedBy = "app")
    private List<Task> tasks;
    @OneToMany(mappedBy = "app")
    @JsonIgnore
    private List<Strategy> strategies;
    @ManyToMany
    @JoinTable(name = "cpt_app_server",
            joinColumns = {@JoinColumn(name = "app_id")},
            inverseJoinColumns = {@JoinColumn(name = "server_id")})
    private List<Server> servers;

    public List<App> getChildren(){
        if (this.children != null&& this.children.size()==0){
            return null;
        }
        return this.children;
    }
}
