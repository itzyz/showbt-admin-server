package com.showbt.entity;

import com.showbt.project.base.model.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cpt_strategy_type")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StrategyType extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String typeName;
    private String className;
}
