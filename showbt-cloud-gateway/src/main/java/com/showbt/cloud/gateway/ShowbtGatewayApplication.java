package com.showbt.cloud.gateway;


import com.showbt.cloud.gateway.route.DbRouteDefinitionRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.gateway.route.InMemoryRouteDefinitionRepository;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.util.pattern.PathPatternParser;

import java.time.Duration;

/**
 * @author iceelor
 * @date 2019年05月14日
 * 服务网关
 */
@ComponentScan(basePackages = {"com.showbt", "com.showbt"})
@EnableFeignClients(basePackages = {"com.showbt"})
@SpringCloudApplication
public class ShowbtGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShowbtGatewayApplication.class, args);
    }

    @Bean
    public InMemoryRouteDefinitionRepository routeDefinitionWriter() {
        return new InMemoryRouteDefinitionRepository();
    }

    @Bean
    public DbRouteDefinitionRepository dbRouteDefinitionRepository() {
        return new DbRouteDefinitionRepository();
    }

    @Bean
    public RestTemplate restTemplateServerRegist(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.setConnectTimeout(Duration.ofMillis(5000))
                .setReadTimeout(Duration.ofMillis(5000))
                .build();
    }

//    /**
//     * 解决post请求报错
//     * @return
//     */
//    @Bean
//    public HiddenHttpMethodFilter hiddenHttpMethodFilter() {
//        return new HiddenHttpMethodFilter() {
//            @Override
//            public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
//                return chain.filter(exchange);
//            }
//        };
//    }

}
