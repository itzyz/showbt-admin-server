package com.showbt.cloud.gateway.service.impl;

import com.showbt.cloud.gateway.entity.ModuleEntity;
import com.showbt.cloud.gateway.repository.ModuleRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class CommonMethod {

    protected HttpHeaders getHeader() {
        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        return headers;
    }

    /**
     * 检查服务是否正常
     *
     * @param url
     * @return
     */
    protected int checkServerStatus(RestTemplate restTemplateServerRegist, String url, String health) {
        int resultCode;
        if (StringUtils.isEmpty(health)) health = "/";
        try {
            ResponseEntity<String> res = restTemplateServerRegist.getForEntity(url + health, null, String.class);
            resultCode = res.getStatusCodeValue();
        } catch (Exception e) {
//            e.printStackTrace();
            resultCode = 400;
        }
        return resultCode;
    }

    public Map<String, List<String[]>> getAllServer(ModuleRepository moduleRepository, RestTemplate restTemplateServerRegist) {
        Map<String, List<String[]>> instanceIdSet = new HashMap();
        List<ModuleEntity> moduleEntities = moduleRepository.findList(1);
        for (ModuleEntity moduleEntity : moduleEntities) {
            final String appName = (moduleEntity.getProject().getLabel() + "-" + moduleEntity.getLabel()).replaceAll("/", "");
            String health = moduleEntity.getHealth();
            if (!moduleEntity.getRealPath().trim().startsWith("lb://")) {
                for (String url : moduleEntity.getRealPath().trim().split(",|，")) {
                    if (url.startsWith("http://") || url.startsWith("https://")) {
                        try {
                            URI uri = new URI(url);
                            int code = checkServerStatus(restTemplateServerRegist, url, health);
                            List<String[]> modules = instanceIdSet.get(appName);
                            if(modules == null){
                                modules = new ArrayList<>();
                            }
                            modules.add(new String[]{appName, uri.getHost(), String.valueOf(uri.getPort()), url, health, String.valueOf(code)});
                            instanceIdSet.put(appName, modules);
                        } catch (URISyntaxException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return instanceIdSet;
    }


}
