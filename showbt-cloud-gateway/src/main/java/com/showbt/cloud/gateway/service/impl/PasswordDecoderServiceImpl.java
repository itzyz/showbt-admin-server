package com.showbt.cloud.gateway.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.http.HttpUtil;
import com.showbt.cloud.common.core.util.TransUtils;
import com.showbt.cloud.gateway.service.PasswordDecoderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author Loris
 * @date Created in 2020/4/21 上午8:34
 * @since 登录密码解密
 */
@Slf4j
@Service
public class PasswordDecoderServiceImpl implements PasswordDecoderService {

    private static final String PASSWORD = "password";
    private static final String KEY_ALGORITHM = "AES";
    @Value("${security.encode.key:1234567812345678}")
    private String encodeKey;

    @Override
    public String decoder(String body) {
        Map<String, String> paramMap = HttpUtil.decodeParamMap(body, CharsetUtil.UTF_8);

        String password = paramMap.get(PASSWORD);
        if (StrUtil.isNotBlank(password)) {
            try {
                password = decryptAES(password, encodeKey);
            } catch (Exception e) {
                log.error("密码解密失败:{}", password);
            }
            paramMap.put(PASSWORD, password.trim());
        }

        return TransUtils.transMapToString(paramMap);
    }

    private static String decryptAES(String data, String pass) {
        AES aes = new AES(Mode.CBC, Padding.NoPadding,
                new SecretKeySpec(pass.getBytes(), KEY_ALGORITHM),
                new IvParameterSpec(pass.getBytes()));
        byte[] result = aes.decrypt(Base64.decode(data.getBytes(StandardCharsets.UTF_8)));
        return new String(result, StandardCharsets.UTF_8);
    }
}
