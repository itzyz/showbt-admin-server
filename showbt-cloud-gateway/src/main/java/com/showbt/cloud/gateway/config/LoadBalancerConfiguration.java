package com.showbt.cloud.gateway.config;

import com.netflix.loadbalancer.IRule;
import com.showbt.cloud.gateway.filter.CustomLoadBalancerClientFilter;
import com.showbt.cloud.gateway.rule.CustomLoadBalancerRule;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.cloud.gateway.config.LoadBalancerProperties;
import org.springframework.cloud.gateway.filter.LoadBalancerClientFilter;
import org.springframework.context.annotation.Bean;

/**
 * todo 这个负载实现方法还有问题
 * 会出现503，还未找出原因。
 */
//@Configuration
public class LoadBalancerConfiguration {

    /**
     * 自定义负载均衡过滤器
     *
     * @param client     LoadBalancerClient
     * @param properties LoadBalancerProperties
     * @return CustomLoadBalancerClientFilter
     */
    @Bean
    public LoadBalancerClientFilter customLoadBalancerClientFilter(LoadBalancerClient client,
                                                                   LoadBalancerProperties properties) {
        return new CustomLoadBalancerClientFilter(client, properties);
    }

    /**
     * 自定义负载均衡规则
     *
     * @return CustomLoadBalancerRule
     */
    @Bean
    public IRule customLoadBalancerRule() {
        return new CustomLoadBalancerRule();
    }

}