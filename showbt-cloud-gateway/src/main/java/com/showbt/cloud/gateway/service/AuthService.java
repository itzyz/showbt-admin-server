package com.showbt.cloud.gateway.service;

import com.showbt.cloud.common.core.util.R;
import com.showbt.entity.Task;

import java.util.Map;

public interface AuthService {
    R<Boolean> checkAuth(Task task, Map<String, Object> params);

    R<Boolean> checkSig(Task task, Map<String, Object> params);

    R<Boolean> checkPermission(Task task, Map<String, Object> params);
}
