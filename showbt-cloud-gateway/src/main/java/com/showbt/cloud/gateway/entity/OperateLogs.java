package com.showbt.cloud.gateway.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.springframework.util.StringUtils;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Table(name = "sbt_operate_log")
@Data
public class OperateLogs {

    /**
     * 日志信息记录表主键id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 操作记录信息
     */
    private String notes;

    /**
     * 操作人ip地址
     */
    private String ip;

    /**
     * 请求的URI地址
     */
    private String uri;

    /**
     * 请求参数记录
     */
    private String params;

    /**
     * 返回结果记录
     */
    private String response;
    /**
     * 操作时间
     */
    private Date createTime;

    /**
     * 操作人id
     */
    private Integer createUser;

    /**
     * 响应时间
     */
    private Date endTime;

    /**
     * 响应结果1成功0失败
     */
    private Integer status;

    public void setAppendResponse(String response) {

        if (!StringUtils.isEmpty(this.response)) {
            this.response = this.response + response;
        } else {
            this.response = response;
        }
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("notes", notes);
        json.put("params", params);
        json.put("response", this.response);
        json.put("status", status);
        json.put("uri", uri);
        json.put("ip", ip);
        return json.toJSONString();
    }
}