package com.showbt.cloud.gateway.service;

/**
 * @author Loris
 * @date Created in 2020/4/21 上午8:34
 * @since
 */
public interface PasswordDecoderService {
    String decoder(String body);
}
