package com.showbt.cloud.gateway.repository;

import com.showbt.cloud.gateway.entity.ServiceInterfaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServerRepository extends JpaRepository<ServiceInterfaceEntity, String> {

    @Query("select s from ServiceInterfaceEntity  s where s.status=?1")
    public List<ServiceInterfaceEntity> findList(int status);
}
