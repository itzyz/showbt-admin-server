package com.showbt.cloud.gateway.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cpt_project", uniqueConstraints = {@UniqueConstraint(columnNames = "label")})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectEntity extends BaseEntity<String> {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /**
     * 项目名称
     */
    private String projectName;
    /**
     * 项目虚拟目录
     */
    private String label;
    /**
     * 项目下对应的模块列表
     */
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("project")
    private List<ModuleEntity> modules;
}
