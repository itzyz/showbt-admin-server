package com.showbt.cloud.gateway.filter.factory;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.showbt.cloud.gateway.route.ParamsCache;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Map;

@Component
public class ShowbtModifyRequestBodyGatewayFilterFactory extends AbstractGatewayFilterFactory implements Ordered {

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            System.out.println("================ShowbtModifyRequestBodyGatewayFilterFactory===============================");
            return handlerParamter(exchange, chain);
        };
    }


    private Mono<Void> handlerParamter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        HttpMethod method = serverHttpRequest.getMethod();
//        String contentType = serverHttpRequest.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
        MediaType mediaType = exchange.getRequest().getHeaders().getContentType();
        ShowbtServerHttpRequestDecorder requestDecorder = new ShowbtServerHttpRequestDecorder();
        String path = serverHttpRequest.getURI().getPath();
        Map<String, String> params = ParamsCache.paramsCacheMap.get(path);
        if (method == HttpMethod.POST) {
            String body = requestDecorder.resolveBodyFromRequest(exchange.getRequest());
            String newBodyStr = "";
            if (MediaType.APPLICATION_FORM_URLENCODED.isCompatibleWith(mediaType)) {
                Map<String, String> paramMap = HttpUtil.decodeParamMap(body, CharsetUtil.UTF_8);
                if (params != null){
                    paramMap.putAll(params);
                }
                newBodyStr = HttpUtil.toParams(paramMap);
            }
            if (MediaType.APPLICATION_JSON.isCompatibleWith(mediaType) || MediaType.APPLICATION_JSON_UTF8.isCompatibleWith(mediaType)) {
                JSONObject obj = JSON.parseObject(body);
                if (params != null) {
                    params.forEach(obj::put);
                }
                newBodyStr = obj.toJSONString();
            }
            ServerHttpRequest newServerHttpRequest = requestDecorder.createPostServerHttpRequest(exchange, newBodyStr);
            return chain.filter(exchange.mutate().request(newServerHttpRequest).build());
        } else if (method == HttpMethod.GET) {
            URI uri = serverHttpRequest.getURI();
            Map<String, String> paramMap = serverHttpRequest.getQueryParams().toSingleValueMap();
            if (params != null){
                paramMap.putAll(params);
            }
            String newParamStr = HttpUtil.toParams(paramMap);

            // 替换查询参数
            URI newUri = UriComponentsBuilder.fromUri(uri).replaceQuery(newParamStr).build(true).toUri();
            ServerHttpRequest newRequest = exchange.getRequest().mutate().uri(newUri).build();
            return chain.filter(exchange.mutate().request(newRequest).build());
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return -999;
    }
}
