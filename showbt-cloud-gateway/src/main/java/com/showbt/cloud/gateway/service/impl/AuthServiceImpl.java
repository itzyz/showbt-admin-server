package com.showbt.cloud.gateway.service.impl;


import com.showbt.cloud.common.core.util.DateFormatUtils;
import com.showbt.cloud.common.core.util.R;
import com.showbt.cloud.gateway.service.AuthService;
import com.showbt.cloud.gateway.util.SigUtils;
import com.showbt.entity.Server;
import com.showbt.entity.Task;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {
    /**
     * 签名验证与权限验证
     *
     * @param task      任务
     * @param params    参数
     * @return
     */
    @Override
    public R<Boolean> checkAuth(Task task, Map<String, Object> params) {
        String path = (String) params.get("path");
        params.remove("path");
        R<Boolean> result = checkSig(task, params);
        params.put("task", task);
        if (result.getData()) {
            params.put("path", path);
            return checkPermission(task, params);
        }
        return result;
    }

    /**
     * 签名验证
     *
     * @param task      任务
     * @param params    参数
     * @return
     */
    @Override
    public R<Boolean> checkSig(Task task, Map<String, Object> params) {
        if (task != null) {

            //是否需要验证sig
            if (task.getIsSign() == 0) {
                return new R<>(Boolean.TRUE);
            }

            if (taskNotInExpiryDate(task)) {
                return new R<>(Boolean.FALSE, "任务不在时效内");
            }

            //参数是否有sig
            if (!params.containsKey("sig")) {
                return new R<>(Boolean.FALSE, "未找到sig");
            }

            //验证sig
            String sig = (String) params.get("sig");

            Map<String, String> sigMap = new HashMap<>();
            for (String key : params.keySet()) {
                if ("sig".equals(key)) {
                    continue;
                }
                sigMap.put(key, (String) params.get(key));
            }

            String rightSig = null;
            try {
                rightSig = SigUtils.encrypt(sigMap, task.getApp().getApikey());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (!sig.equals(rightSig)) {
                return new R<>(Boolean.FALSE, "签名错误!");
            }
            return new R<>(Boolean.TRUE);
        }
        return new R<>(Boolean.FALSE, "请检查taskId");
    }

    /**
     * 任务是否在时效内
     *
     * @param task　任务
     * @return
     */
    private boolean taskNotInExpiryDate(Task task) {
        LocalDate now = LocalDate.now();
        LocalDate effectiveTimeDate = DateFormatUtils.transDateToLocalDate(task.getEffectiveTime());
        LocalDate expiryTimeDate = DateFormatUtils.transDateToLocalDate(task.getExpiryTime());
        return now.isAfter(expiryTimeDate) || now.isBefore(effectiveTimeDate);
    }

    /**
     * 权限验证
     *
     * @param task      任务
     * @param params    参数
     * @return
     */
    @Override
    public R<Boolean> checkPermission(Task task, Map<String, Object> params) {
        if (task != null) {

            List<String> permissions = getPermissions(task.getApp().getServers());
            if(params.get("path") != null) {
                String[] split = ((String) params.get("path")).trim().split("/");
                String url = "/" + split[1] + "/" + split[2] + "/" + split[3];
                if (permissions.contains(url)) {
                    return new R<>(Boolean.TRUE);
                }
            }
            return new R<>(Boolean.FALSE, "权限不足");
        }
        return new R<>(Boolean.FALSE, "请检查taskId");
    }

    /**
     * 获取任务对应应用拥有的权限集合
     *
     * @param servers   应用可以使用的服务
     * @return          应用权限集合
     */
    private List<String> getPermissions(List<Server> servers) {
        return servers.parallelStream().map(server -> {
            String projectLabel = server.getModule().getProject().getLabel();
            String moduleLabel = server.getModule().getLabel();
            String label = server.getLabel();
            return projectLabel + moduleLabel + label;
        }).collect(Collectors.toList());
    }
}
