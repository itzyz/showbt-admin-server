package com.showbt.cloud.gateway.route;
import java.util.HashMap;
import java.util.Map;

/**
 * 配置路由时会加载接口的固定参数
 * 该类用于缓存这些固定参数，在转发接口前会把固定参数追加到请求体中
 */
public class ParamsCache {
    /**
     * 用于存放路由的固定参数值
     * key:路由地址
     * value:参数字典->key:参数名称 ， value:参数值
     */
    public static Map<String, Map<String, String>> paramsCacheMap = new HashMap<>();
}
