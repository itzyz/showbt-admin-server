package com.showbt.cloud.gateway.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Producer;
import com.showbt.cloud.common.core.constant.CommonConstants;
import com.showbt.cloud.common.core.exception.ValidateCodeException;
import com.showbt.cloud.common.core.util.R;
import com.showbt.cloud.gateway.service.ValidateCodeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.FastByteArrayOutputStream;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {
    @Autowired
    private Producer producer;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public R<JSONObject> generateValidateCode() {
        //生成验证码
        String text = producer.createText();
        BufferedImage image = producer.createImage(text);

        //保存验证码信息
        String randomStr = UUID.randomUUID().toString().replaceAll("-", "");
        redisTemplate.opsForValue().set(CommonConstants.DEFAULT_CODE_KEY + randomStr, text, 60, TimeUnit.SECONDS);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try {
            ImageIO.write(image, "jpeg", os);
        } catch (IOException e) {
            log.error("ImageIO write err", e);
            return new R(e);
        }

        JSONObject obj = new JSONObject();
        try {
            obj.put("image", "data:image/jpg;base64,"+ new String(Base64.getEncoder().encode(os.toByteArray()),"UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        obj.put("validateKey", randomStr);

        return new R().success(obj,"");
    }

    @Override
    public R checkValidateCode(String randomStr, String code) {
        if (StrUtil.isBlank(code)) {
            return new R().fail("验证码不能为空");
        }

        if (StrUtil.isBlank(randomStr)) {
            return new R().fail("验证码Key不能为空");
        }

        String key = CommonConstants.DEFAULT_CODE_KEY + randomStr;
        if (!redisTemplate.hasKey(key)) {
            return new R().fail("验证码不合法");
        }

        Object codeObj = redisTemplate.opsForValue().get(key);

        if (codeObj == null) {
            return new R().fail("验证码不合法");
        }

        String saveCode = codeObj.toString();
        if (StrUtil.isBlank(saveCode)) {
            redisTemplate.delete(key);
            return new R().fail("验证码不合法");
        }

        if (!StrUtil.equals(saveCode, code)) {
            redisTemplate.delete(key);
            return new R().fail("验证码不合法");
        }
        redisTemplate.delete(key);
        return new R().success("验证码合法");
    }
}
