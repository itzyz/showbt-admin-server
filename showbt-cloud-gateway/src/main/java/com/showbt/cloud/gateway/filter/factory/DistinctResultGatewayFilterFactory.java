package com.showbt.cloud.gateway.filter.factory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class DistinctResultGatewayFilterFactory
        extends AbstractGatewayFilterFactory<DistinctResultGatewayFilterFactory.Config> {

    private static final String DISTINCT_FIELD_KEY = "distinctField";

    public DistinctResultGatewayFilterFactory() {
        super(Config.class);
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList(DISTINCT_FIELD_KEY);
    }

    @Override
    public GatewayFilter apply(Config config) {
        return new DistinctResultGatewayFilter(config);
    }

    @AllArgsConstructor
    public static class DistinctResultGatewayFilter implements GatewayFilter, Ordered {

        private Config config;

        @Override
        public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
            return chain.filter(exchange).then(Mono.fromRunnable(() -> {
                String field = config.getDistinctField();
                String[] split = field.split("\\|");
                for (String s : split) {
                    Map<String, String> map = new HashMap<>();
                    log.info("结果去重,去重字段: " + s);
                }
            }));
        }

        @Override
        public int getOrder() {
            return 0;
        }
    }

    @Setter
    @Getter
    public static class Config {
        private String distinctField;
    }
}
