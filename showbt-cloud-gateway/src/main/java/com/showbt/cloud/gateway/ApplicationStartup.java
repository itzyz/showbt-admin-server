package com.showbt.cloud.gateway;

import com.showbt.cloud.gateway.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Timer;
import java.util.TimerTask;

@Component
public class ApplicationStartup implements ApplicationRunner {

    private final ServerService serverService;

    public ApplicationStartup(ServerService serverService) {
        this.serverService = serverService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        serverService.loadRouteDefinition();
    }
}