package com.showbt.cloud.gateway.filter.log;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.NettyWriteResponseFilter;
import org.springframework.cloud.gateway.support.DefaultServerRequest;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.ORIGINAL_RESPONSE_CONTENT_TYPE_ATTR;

@Slf4j
@Component
public class DefaultLogFilter implements GlobalFilter, Ordered {

    @Value("${xh.auto.print.request}")
    private boolean printRequestParams;
    @Value("${xh.auto.print.response}")
    private boolean printResponseResult;

    public static final String REQUEST = "request";
    public static final String RESPONSE = "response";

    /**
     * 日志信息
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        long startTime = System.currentTimeMillis();
        String requestId = UUID.randomUUID().toString().replace("-", "");
        String trace = exchange.getRequest().getHeaders().getFirst("trace");
        ServerRequest serverRequest = new DefaultServerRequest(exchange);
        HttpMethod method = exchange.getRequest().getMethod();
        if (HttpMethod.GET.equals(method) || HttpMethod.DELETE.equals(method)) {
            printLog(exchange.getRequest(), exchange.getResponse(), startTime, trace, requestId, "", REQUEST);
            return chain.filter(exchange)
                    .then(Mono.fromRunnable(() -> printLog(exchange.getRequest(), exchange.getResponse(), startTime, trace, requestId,"", RESPONSE)));
        } else {
            return postHandle(serverRequest, exchange, chain, trace, startTime, requestId);
        }
    }

    @Override
    public int getOrder() {
        //在NettyWriteResponseFilter之前
        return NettyWriteResponseFilter.WRITE_RESPONSE_FILTER_ORDER - 20;
    }

    protected Mono<Void> postHandle(ServerRequest serverRequest, ServerWebExchange exchange, GatewayFilterChain chain, String trace, long startTime, String requestId) {
        return serverRequest.bodyToMono(String.class).flatMap(reqBody -> {
            //重写原始请求
            ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(exchange.getRequest()) {
                @Override
                public HttpHeaders getHeaders() {
                    HttpHeaders httpHeaders = new HttpHeaders();
                    httpHeaders.putAll(super.getHeaders());
                    return httpHeaders;
                }

                @Override
                public Flux<DataBuffer> getBody() {
                    printLog(exchange.getRequest(), exchange.getResponse(), startTime, trace, requestId, reqBody, REQUEST);
                    return Flux.just(reqBody).map(bx -> exchange.getResponse().bufferFactory().wrap(bx.getBytes()));
                }
            };
            //重写原始响应
            BodyHandlerServerHttpResponseDecorator responseDecorator = new BodyHandlerServerHttpResponseDecorator(
                    initBodyHandler(exchange, startTime, requestId), exchange.getResponse());
            return chain.filter(exchange.mutate().request(decorator).response(responseDecorator).build());
        });
    }

    /**
     * 响应body处理，添加响应的打印
     *
     * @param exchange
     * @param startTime
     * @return
     */
    protected BodyHandlerFunction initBodyHandler(ServerWebExchange exchange, long startTime, String requestId) {
        return (resp, body) -> {
            //拦截
            String trace = exchange.getRequest().getHeaders().getFirst("trace");
            HttpHeaders httpHeaders = new HttpHeaders();
            String contentType = exchange.getAttribute(ORIGINAL_RESPONSE_CONTENT_TYPE_ATTR);
            if (StringUtils.isNotBlank(contentType)) {
                httpHeaders.setContentType(MediaType.valueOf(contentType));
            }
            DefaultClientResponseAdapter clientResponseAdapter = new DefaultClientResponseAdapter(body, httpHeaders);
            Mono<String> bodyMono = clientResponseAdapter.bodyToMono(String.class);
            return bodyMono.flatMap(respBody -> {
                printLog(exchange.getRequest(), resp, startTime, trace, requestId, respBody, RESPONSE);
                return resp.writeWith(Flux.just(respBody).map(bx -> resp.bufferFactory().wrap(bx.getBytes())));
            }).then();
        };
    }

    private void printLog(ServerHttpRequest request, ServerHttpResponse response, long startTime, String trace, String requestId, String body, String type) {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("request_id", requestId);
        jsonObj.put("type", type);
        jsonObj.put("trace", trace);
        jsonObj.put("uri", request.getURI());
        jsonObj.put("method", request.getMethodValue());
        if (REQUEST.equalsIgnoreCase(type)) {
            jsonObj.put("header", request.getHeaders());
            jsonObj.put("request_params", request.getQueryParams());
            if (printRequestParams) {
                jsonObj.put("request_body", body);
            }
        } else {
            jsonObj.put("response_code", response.getStatusCode().value());
            jsonObj.put("header", response.getHeaders());
            if(printResponseResult) {
                jsonObj.put("response_body", body);
            }
        }
        jsonObj.put("time", System.currentTimeMillis() - startTime);
        //打印返回响应日志
        log.info(jsonObj.toJSONString());
    }
}