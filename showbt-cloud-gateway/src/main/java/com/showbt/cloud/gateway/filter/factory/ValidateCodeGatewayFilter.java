package com.showbt.cloud.gateway.filter.factory;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.showbt.cloud.common.core.config.FilterIgnorePropertiesConfig;
import com.showbt.cloud.common.core.constant.CommonConstants;
import com.showbt.cloud.common.core.constant.SecurityConstants;
import com.showbt.cloud.common.core.util.R;
import com.showbt.cloud.common.core.util.WebUtils;
import com.showbt.cloud.gateway.service.ValidateCodeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * 验证码处理
 */
@Slf4j
@Component
@AllArgsConstructor
public class ValidateCodeGatewayFilter extends AbstractGatewayFilterFactory {
    private final ObjectMapper objectMapper;
    private final FilterIgnorePropertiesConfig filterIgnorePropertiesConfig;
    private ValidateCodeService validateCodeService;

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();

            // 不是登录请求，直接向下执行
            if (!StrUtil.containsAnyIgnoreCase(request.getURI().getPath()
                    , SecurityConstants.OAUTH_TOKEN_URL)) {
                return chain.filter(exchange);
            }
            String grantType;
            String code;
            String randomStr;
            String bodyStr = "";
            HttpMethod method = request.getMethod();
            ShowbtServerHttpRequestDecorder requestDecorder = new ShowbtServerHttpRequestDecorder();
            if (method == HttpMethod.POST) {
                //从post请求中获取请求体
                bodyStr = requestDecorder.resolveBodyFromRequest(request);
                Map<String, String> paramMap = HttpUtil.decodeParamMap(bodyStr, CharsetUtil.UTF_8);
                grantType = paramMap.get("grant_type");
                code = paramMap.get("code");
                randomStr = paramMap.get("randomStr");
            } else {
                grantType = request.getQueryParams().getFirst("grant_type");
                code = request.getQueryParams().getFirst("code");
                randomStr = request.getQueryParams().getFirst("randomStr");
            }

            // 刷新token，直接向下执行
            if (StrUtil.equals(SecurityConstants.REFRESH_TOKEN, grantType)) {
                return getVoidMono(exchange, chain, bodyStr, method, requestDecorder);
            }

            // 终端设置不校验， 直接向下执行
            try {
                String[] clientInfos = WebUtils.getClientId(request);
                if (filterIgnorePropertiesConfig.getClients().contains(clientInfos[0])) {
//                    return chain.filter(exchange);
                    return getVoidMono(exchange, chain, bodyStr, method, requestDecorder);
                }

                //校验验证码
                R res = validateCodeService.checkValidateCode(randomStr, code);
                if (res.getCode() == 200) {
//                    return chain.filter(exchange);
                    return getVoidMono(exchange, chain, bodyStr, method, requestDecorder);
                } else {
                    ServerHttpResponse response = exchange.getResponse();
                    response.setStatusCode(HttpStatus.resolve(201));
                    HttpHeaders httpHeaders = response.getHeaders();
                    //返回数据格式
                    httpHeaders.setContentType(MediaType.APPLICATION_JSON);
                    return response.writeWith(Mono.just(response.bufferFactory().wrap(objectMapper.writeValueAsBytes(res))));
                }
            } catch (Exception e) {
                ServerHttpResponse response = exchange.getResponse();
                response.setStatusCode(HttpStatus.PRECONDITION_REQUIRED);
                try {
                    return response.writeWith(Mono.just(response.bufferFactory()
                            .wrap(objectMapper.writeValueAsBytes(
                                    R.builder().msg(e.getMessage())
                                            .code(CommonConstants.FAIL).build()))));
                } catch (JsonProcessingException e1) {
                    log.error("对象输出异常", e1);
                }
            }
//            return chain.filter(exchange);
            return getVoidMono(exchange, chain, bodyStr, method, requestDecorder);
        };
    }

    private Mono<Void> getVoidMono(ServerWebExchange exchange, GatewayFilterChain chain, String bodyStr, HttpMethod method, ShowbtServerHttpRequestDecorder requestDecorder) {
        ServerHttpRequest newServerHttpRequest = requestDecorder.createPostServerHttpRequest(exchange, bodyStr);
        if (method == HttpMethod.POST) {
            return chain.filter(exchange.mutate().request(newServerHttpRequest).build());
        } else {
            return chain.filter(exchange);
        }
    }

}
