package com.showbt.cloud.gateway.route;

import com.showbt.cloud.common.core.util.TransUtils;
import com.showbt.cloud.gateway.entity.ServiceInterfaceEntity;
import com.showbt.cloud.gateway.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.cloud.gateway.support.NotFoundException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 定义DbRouteDefinitionRepository类，
 * 实现RouteDefinitionRepository接口的getRouteDefinitions方法，
 * 获取从数据库里面装载的路由配置，并实现save和delete其他方法。
 * todo 注意：每次请求都会调用getRouteDefinitions，去查找数据库，需要做性能优化，否则会出现瓶颈
 */
public class DbRouteDefinitionRepository implements RouteDefinitionRepository {
//    @Autowired
//    private GatewayDefineService gatewayDefineService;

    @Autowired
    private ServerService serverService;

    @Override
    public Flux<RouteDefinition> getRouteDefinitions() {
        Map<String, RouteDefinition> routes = new LinkedHashMap<>();
        List<ServiceInterfaceEntity> serverEntityList = serverService.findList(1);
        try {
            for (ServiceInterfaceEntity serverEntity : serverEntityList) {
                RouteDefinition definition = serverService.interfaceEntityToRouteDefinition(serverEntity);
                routes.put(definition.getId(), definition);
                StringBuilder paramKey = new StringBuilder();
                paramKey.append(serverEntity.getModule().getProject().getLabel())
                        .append(serverEntity.getModule().getLabel())
                        .append(serverEntity.getLabel());
                if (serverEntity.getFixedParams() != null) {
                    ParamsCache.paramsCacheMap.put(paramKey.toString(), TransUtils.transJsonToStrMap(serverEntity.getFixedParams()));
                }
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return Flux.empty();
        }
        return Flux.fromIterable(routes.values());

        /*try {
            List<GatewayDefine> gatewayDefineList = gatewayDefineService.findAll();
            Map<String, RouteDefinition> routes = new LinkedHashMap<String, RouteDefinition>();
            for (GatewayDefine gatewayDefine : gatewayDefineList) {getRouteDefinitions
                RouteDefinition definition = gatewayDefineService.gatewayDefineToRouteDefinition(gatewayDefine);
                routes.put(definition.getId(), definition);
            }
            return Flux.fromIterable(routes.values());
        } catch (Exception e) {
            e.printStackTrace();
            return Flux.empty();
        }*/
    }

    @Override
    public Mono<Void> save(Mono<RouteDefinition> route) {
        return route.flatMap(r -> {
            try {
//                gatewayDefineService.save(gatewayDefineService.routeDefinitionToGatewayDefine(r));
                return Mono.empty();

            } catch (Exception e) {
                e.printStackTrace();
                return Mono.defer(() -> Mono.error(new NotFoundException("RouteDefinition save error: " + r.getId())));
            }
        });
    }

    @Override
    public Mono<Void> delete(Mono<String> routeId) {
        return routeId.flatMap(id -> {
            try {
//                gatewayDefineService.deleteById(id);
                return Mono.empty();
            } catch (Exception e) {
                e.printStackTrace();
                return Mono.defer(() -> Mono.error(new NotFoundException("RouteDefinition delete error: " + routeId)));
            }
        });
    }
}