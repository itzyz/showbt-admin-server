package com.showbt.cloud.gateway.filter.factory;

import io.netty.buffer.ByteBufAllocator;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.NettyDataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.nio.CharBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;

/**
 * ServerHttpRequest 请求体解码器
 * 用于处理post请求时，解析body请求参数
 */
public class ShowbtServerHttpRequestDecorder {
    public String resolveBodyFromRequest(ServerHttpRequest serverHttpRequest) {
        //获取请求体
        Flux<DataBuffer> body = serverHttpRequest.getBody();
        AtomicReference<String> bodyRef = new AtomicReference<>();

        body.subscribe(buffer -> {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(buffer.asByteBuffer());
            DataBufferUtils.release(buffer);
            bodyRef.set(charBuffer.toString());
        });
        return bodyRef.get();
    }

    /**
     * 将字符串转换成DataBuffer
     *
     * @param body
     * @return
     */
    private DataBuffer stringToBuffer(String body) {
        byte[] bytes = body.getBytes(StandardCharsets.UTF_8);
        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
        buffer.write(bytes);
        return buffer;
    }

    /**
     * 由于post请求中的body只能读取一次，修改请求参数后，需要重新封装ServerHttpRequest对象
     * @param exchange
     * @param newBodyStr 新的请求体
     * @return
     */
    public ServerHttpRequest createPostServerHttpRequest(ServerWebExchange exchange, String newBodyStr){
        //全局参数解析
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        URI uri = serverHttpRequest.getURI();
        URI newUri = UriComponentsBuilder.fromUri(uri).build(true).toUri();
        DataBuffer newBodyFlux = stringToBuffer(newBodyStr);
        Flux<DataBuffer> bodyFlux = Flux.just(newBodyFlux);

        //定义新的消息头
        HttpHeaders newHttpHeaders = new HttpHeaders();
        newHttpHeaders.putAll(serverHttpRequest.getHeaders());

        //由于修改了参数，需要重新设置content_length,长度为字节长度，不是字符串长度
        int newContentLength = newBodyStr.getBytes().length;
        newHttpHeaders.remove(HttpHeaders.CONTENT_LENGTH);
        newHttpHeaders.setContentLength(newContentLength);
        String contentType = serverHttpRequest.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
        //设置conent_type
        if (StringUtils.isNotBlank(contentType)) {
            newHttpHeaders.set(HttpHeaders.CONTENT_TYPE, contentType);
        }

        // body只能订阅一次，所以需要重新封装请求body
        ServerHttpRequest newServerHttpRequest = serverHttpRequest.mutate().uri(newUri).build();
        newServerHttpRequest = new ServerHttpRequestDecorator(newServerHttpRequest) {
            @Override
            public HttpHeaders getHeaders() {
                long length = newHttpHeaders.getContentLength();
                HttpHeaders headers = new HttpHeaders();
                headers.putAll(super.getHeaders());
                if (length > 0) {
                    headers.setContentLength(length);
                } else {
                    headers.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                }
                return headers;
            }

            @Override
            public Flux<DataBuffer> getBody() {
                return bodyFlux;
            }
        };
        newServerHttpRequest.mutate().header(HttpHeaders.CONTENT_LENGTH, Integer.toString(newBodyStr.length()));
        return newServerHttpRequest;
    }
}
