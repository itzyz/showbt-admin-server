package com.showbt.cloud.gateway.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "cpt_server_rule")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceInterfaceRuleEntity {
    @EmbeddedId
    private ServiceInterfaceRulePKEntity srpk;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false, columnDefinition = "Text")
    private String args;
    /**
     * filter或规则排序字段
     */
    private Integer filterOrRuleOrder = 0;
}
