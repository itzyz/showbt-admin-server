package com.showbt.cloud.gateway.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * 项目模块实体类
 */
@Entity
@Table(name = "cpt_module", uniqueConstraints = {@UniqueConstraint(columnNames = {"label", "p_id"})})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ModuleEntity extends BaseEntity<String> {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /**
     * 模块名称
     */
    @Column(length = 120)
    private String moduleName;
    /**
     * 模块虚拟目录
     */
    @Column(length = 120)
    private String label;
    /**
     * 模块对应的真实地址
     */
    private String realPath;
    /**
     * 心跳地址
     */
    private String health;
    /**
     * 模块对应的服务接口列表
     */
    @OneToMany(mappedBy = "module", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("module")
    private List<ServiceInterfaceEntity> servers;
    @ManyToOne
    @JoinColumn(name = "p_id")
    @JsonIgnoreProperties("modules")
    private ProjectEntity project;
}
