package com.showbt.cloud.gateway.filter.factory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.showbt.cloud.common.core.constant.CommonConstants;
import com.showbt.cloud.common.core.util.R;
import com.showbt.cloud.common.core.util.TransUtils;
import com.showbt.cloud.gateway.filter.DecorderServerHttpRequestDecorator;
import com.showbt.cloud.gateway.service.AuthService;
import com.showbt.entity.Server;
import com.showbt.entity.Task;
import com.showbt.feign.RemoteTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 权限验证
 *
 * @author loris
 */
@Slf4j
@Component
public class AuthGatewayFilterFactory
        extends AbstractGatewayFilterFactory {

    @Autowired
    private AuthService authService;
    @Autowired
    private RemoteTaskService remoteTaskService;

    @Override
    @SuppressWarnings("unchecked")
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            ServerHttpRequest serverHttpRequest = exchange.getRequest();
            if(HttpMethod.GET == exchange.getRequest().getMethod()){
                ServerHttpResponse serverHttpResponse = exchange.getResponse();
                serverHttpResponse.setStatusCode(HttpStatus.METHOD_NOT_ALLOWED);
                return serverHttpResponse.setComplete();
            }
            ServerHttpRequest decorator = new DecorderServerHttpRequestDecorator(serverHttpRequest);
            AtomicReference<String> bodyRef = new AtomicReference<>();
            //buffer()为了解决Flux中body太长会分片的问题
            decorator.getBody().buffer().subscribe(dataBuffers -> {
                List<String> bodyList = new ArrayList<>();
                dataBuffers.forEach(dataBuffer -> bodyList.add(TransUtils.transDataBufferToString(dataBuffer)));
                bodyRef.set(Joiner.on("").join(bodyList));
            });
            String body = bodyRef.get();
            DataBufferFactory bufferFactory = exchange.getResponse().bufferFactory();
            if (body != null) {
                HttpHeaders headers = exchange.getRequest().getHeaders();

                if (!StringUtils.isEmpty(body)) {
                    MediaType contentType = headers.getContentType();

                    Map<String, Object> checkMap = null;

                    if (MediaType.APPLICATION_JSON.equals(contentType)) {
                        checkMap = TransUtils.transJsonToMap(body);
                    } else {
                        checkMap = TransUtils.transStringToMap(body);
                    }

                    if (checkMap != null && checkMap.size() > 0) {
                        R<Boolean> result = null;
                        String taskId = (String) checkMap.get("taskId");
                        if (org.apache.commons.lang.StringUtils.isNotBlank(taskId)) {
                            checkMap.put("path", exchange.getAttribute("uri"));
                            R<Task> r = remoteTaskService.findOne(taskId);
                            if (r != null && r.getData() != null) {
                                result = authService.checkAuth(r.getData(), checkMap);
                            } else {
                                result = new R<>(Boolean.FALSE, "任务id不存在，请核对taskId参数值。");
                            }
                        } else {
                            result = new R<>(Boolean.FALSE, "任务id不能空，请求确认后再试。");
                        }

                        if (result.getData()) {
                            decorator = requestWrapper(decorator, checkMap);
                            return chain.filter(exchange.mutate().request(decorator).build());
                        }
                        exchange.getResponse().setStatusCode(HttpStatus.UNAUTHORIZED);

                        try {
                            ObjectMapper objectMapper = new ObjectMapper();
                            DataBuffer buffer = TransUtils.transStringToDataBuffer(bufferFactory,
                                    objectMapper.writeValueAsString(result));
                            return exchange.getResponse().writeWith(Mono.just(buffer));
                        } catch (JsonProcessingException e) {
                            log.error(e.getMessage());
                        }
                    }
                }
            }

            exchange.getResponse().setStatusCode(HttpStatus.PRECONDITION_FAILED);

            JSONObject json = new JSONObject();
            json.put("code", CommonConstants.SUCCESS);
            json.put("msg", "参数为空或不合法");
            json.put("data", Boolean.FALSE);
            DataBuffer buffer = TransUtils.transStringToDataBuffer(bufferFactory, json.toString());
            return exchange.getResponse().writeWith(Mono.just(buffer));
        };
    }

    /**
     * 根据服务参数重新封装request
     * 用于post转化为get或其他协议
     *
     * @param request   请求
     * @return          重新封装之后的请求
     */
    private ServerHttpRequest requestWrapper(ServerHttpRequest request, Map<String, Object> checkMap) {
        String rawPath = (String) checkMap.get("path");
        String[] pathArr = StringUtils.tokenizeToStringArray(rawPath, "/");
        Task task = (Task) checkMap.get("task");
        Server server = getServer(task.getApp().getServers(), pathArr);
        if (server != null && server.getRequestParamKeys() != null) {
            String method = server.getMethod();
            if ("GET".equalsIgnoreCase(method)) {
                return request.mutate()
                        .method(HttpMethod.valueOf(method))
                        .path(request.getPath() +
                                getRequestParams(server.getFixedParams(), server.getRequestParamKeys(), (String) checkMap.get("param")))
                        .build();
            }
        }
        return request;
    }

    /**
     * 从所有的服务中拿到当前请求对应的服务
     * 由于 label 字段的唯一性，故如果有直接跳出循环即可
     *
     * @param servers   应用的所有权限服务
     * @param pathArr   地址
     * @return          该次请求对应的服务
     */
    private Server getServer(List<Server> servers, String[] pathArr) {

        Server targetServer = null;
        for (Server server : servers) {
            String serverLabel = server.getLabel();
            String moduleLabel = server.getModule().getLabel();
            String projectLabel = server.getModule().getProject().getLabel();
            if (projectLabel.equals("/" + pathArr[0]) && moduleLabel.equals("/" + pathArr[1]) && serverLabel.equals("/" + pathArr[2])) {
                targetServer = server;
                break;
            }
        }

        return targetServer;
    }

    /**
     * 将数据库中的配置与用户请求中的param字段转化为请求的参数
     *
     * @param fixedParams       数据库中的固定参数字段
     * @param requestParamKeys  需要从用户参数中获取的字段
     * @param requestParams     用户请求参数
     * @return                  最终转发的请求参数
     */
    private String getRequestParams(String fixedParams, String requestParamKeys, String requestParams) {
        Map<String, Object> paramMap = new HashMap<>();

        //转化固定参数
        if (fixedParams != null) {
            JSONObject fxpJson = JSON.parseObject(fixedParams);
            for (String key : fxpJson.keySet()) {
                paramMap.put(key, fxpJson.get(key));
            }
        }

        //转化请求参数
        JSONArray rpkArr = JSON.parseArray(requestParamKeys);
        JSONObject rpObj = JSON.parseObject(requestParams);
        for (Object requestParamKey : rpkArr) {
            String key = (String) requestParamKey;
            if (rpObj.containsKey(key))    {
                paramMap.put(key, rpObj.get(requestParamKey));
            }
        }
        return convertParams(paramMap);
    }

    /**
     * 将参数转化为get请求参数
     *
     * @param paramMap
     * @return
     */
    private String convertParams(Map<String, Object> paramMap) {
        StringBuilder sb = new StringBuilder("?");
        Iterator<String> fxpIter = paramMap.keySet().iterator();
        while (fxpIter.hasNext()) {
            String key = fxpIter.next();
            sb.append(key).append("=").append(paramMap.get(key));
            if (fxpIter.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }
}
