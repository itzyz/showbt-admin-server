package com.showbt.cloud.gateway.repository;

import com.showbt.cloud.gateway.entity.ModuleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModuleRepository extends JpaRepository<ModuleEntity, String> {
    @Query("select m from ModuleEntity  m where m.status=?1")
    List<ModuleEntity> findList(int status);
}
