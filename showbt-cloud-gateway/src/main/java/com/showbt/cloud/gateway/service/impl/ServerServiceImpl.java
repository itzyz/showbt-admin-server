package com.showbt.cloud.gateway.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.showbt.cloud.gateway.entity.ModuleEntity;
import com.showbt.cloud.gateway.entity.RuleDictEntity;
import com.showbt.cloud.gateway.entity.ServiceInterfaceEntity;
import com.showbt.cloud.gateway.entity.ServiceInterfaceRuleEntity;
import com.showbt.cloud.gateway.repository.ServerRepository;
import com.showbt.cloud.gateway.route.DbRouteDefinitionRepository;
import com.showbt.cloud.gateway.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.event.RefreshRoutesEvent;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.filter.factory.RewritePathGatewayFilterFactory;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class ServerServiceImpl implements ServerService {

    @Autowired
    private ServerRepository serverRepository;

    @Autowired
    private ApplicationEventPublisher publisher;
    @Autowired
    private DbRouteDefinitionRepository routeDefinitionWriter;

    @Override
    public List<ServiceInterfaceEntity> findAll() {
        return serverRepository.findAll();
    }

    @Override
    public List<ServiceInterfaceEntity> findList(int status) {
        return serverRepository.findList(status);
    }

    @Override
    public ServiceInterfaceEntity findById(String serverId) {
        return serverRepository.findById(serverId).orElse(null);
    }

    @Override
    public String loadRouteDefinition() {
        try {
            List<ServiceInterfaceEntity> interfaceEntityList = serverRepository.findList(1);
            if (interfaceEntityList == null) {
                return "none route defined";
            }
            for (ServiceInterfaceEntity interfaceEntity : interfaceEntityList) {
                RouteDefinition definition = interfaceEntityToRouteDefinition(interfaceEntity);
                //todo 这个订阅没弄明白是干什么的subscribe()
                routeDefinitionWriter.save(Mono.just(definition)).subscribe();
                this.publisher.publishEvent(new RefreshRoutesEvent(this));
            }
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
            return "failure";
        }
    }

    /**
     * 将接口转换成路由
     *
     * @param interfaceEntity
     * @return
     * @throws URISyntaxException
     */
    public RouteDefinition interfaceEntityToRouteDefinition(ServiceInterfaceEntity interfaceEntity) throws URISyntaxException {
        RouteDefinition routeDefinition = new RouteDefinition();
        routeDefinition.setId(interfaceEntity.getId());
        routeDefinition.setUri(getBalanceUri(interfaceEntity));

        JSONArray filterArray = new JSONArray();
        JSONArray predicateArray = new JSONArray();
        for (ServiceInterfaceRuleEntity sire : interfaceEntity.getConfigs()) {
            RuleDictEntity rde = sire.getSrpk().getRuleDict();
            JSONObject object = new JSONObject();
            object.put("args", JSON.parse(sire.getArgs().replace("\\", "")));
            object.put("name", rde.getRuleName());
            if (rde.getRuleType().equalsIgnoreCase("predicate")) {
                predicateArray.add(object);
            } else if (rde.getRuleType().equalsIgnoreCase("filter")) {
                filterArray.add(object);
            }
        }
        predicateArray.add(getPathPredicate(interfaceEntity));
        filterArray.add(getRewritePathFilter(interfaceEntity));
        List<PredicateDefinition> predicateDefinitionList = JSON.parseArray(predicateArray.toJSONString(), PredicateDefinition.class);
        List<FilterDefinition> filterDefinitionList = JSON.parseArray(filterArray.toJSONString(), FilterDefinition.class);
        if (predicateDefinitionList != null) {
            routeDefinition.setPredicates(predicateDefinitionList);
        }
        if (filterDefinitionList != null) {
            routeDefinition.setFilters(filterDefinitionList);
        }
        return routeDefinition;
    }

    /**
     * @param routeDefinition
     * @return
     */
    @Override
    public ServiceInterfaceEntity routeDefinitionToInterfaceEntity(RouteDefinition routeDefinition) {
        ServiceInterfaceEntity interfaceEntity = findById(routeDefinition.getId());
        if (interfaceEntity == null) {
            interfaceEntity = new ServiceInterfaceEntity();
            interfaceEntity.setId(routeDefinition.getId());
        }
        ModuleEntity moduleEntity = new ModuleEntity();
        moduleEntity.setRealPath(routeDefinition.getUri().getPath());
        interfaceEntity.setModule(moduleEntity);
        return interfaceEntity;
    }

    /**
     * Path predicate
     *
     * @param serverEntity
     * @return
     */
    private JSONObject getPathPredicate(ServiceInterfaceEntity serverEntity) {
        JSONObject pathJson = new JSONObject();
        pathJson.put("name", "Path");
        JSONObject argsJson = new JSONObject();
        String pattern = serverEntity.getModule().getProject().getLabel() + serverEntity.getModule().getLabel();
        if (serverEntity.getLabel().trim().equalsIgnoreCase("/")) {
            pattern += "/**";
        } else {
            pattern += serverEntity.getLabel() + "/**";
        }
        argsJson.put("pattern", pattern);
        pathJson.put("args", argsJson);
        return pathJson;
    }

    /**
     * RewritePath Filter
     *
     * @param serverEntity
     * @return
     */
    private JSONObject getRewritePathFilter(ServiceInterfaceEntity serverEntity) {
        JSONObject rewritePath = new JSONObject();
        rewritePath.put("name", "RewritePath");
        JSONObject argsJson = new JSONObject();
        String regexp = serverEntity.getModule().getProject().getLabel() + serverEntity.getModule().getLabel() + serverEntity.getLabel();
        String replacement = serverEntity.getRealPath();
        argsJson.put(RewritePathGatewayFilterFactory.REGEXP_KEY, regexp);
        argsJson.put(RewritePathGatewayFilterFactory.REPLACEMENT_KEY, replacement);
        rewritePath.put("args", argsJson);
        return rewritePath;
    }

    /**
     * 获取负载地址
     *
     * @param interfaceEntity
     * @return
     * @throws URISyntaxException
     */
    private URI getBalanceUri(ServiceInterfaceEntity interfaceEntity) throws URISyntaxException {
        final String appName = (interfaceEntity.getModule().getProject().getLabel() + "-" + interfaceEntity.getModule().getLabel()).replaceAll("/", "");
        URI resultUrl = new URI("lb://" + appName);
        if (interfaceEntity.getModule().getRealPath().trim().startsWith("lb://")) {
            return new URI(interfaceEntity.getModule().getRealPath().trim().toLowerCase());
        }
        return resultUrl;
    }
}



