package com.showbt.cloud.gateway.service;

import com.showbt.cloud.gateway.entity.ServiceInterfaceEntity;
import org.springframework.cloud.gateway.route.RouteDefinition;

import java.net.URISyntaxException;
import java.util.List;

public interface ServerService {

    List<ServiceInterfaceEntity> findAll();

    List<ServiceInterfaceEntity> findList(int status);

    ServiceInterfaceEntity findById(String serverId);

    String loadRouteDefinition() throws Exception;

    /**
     * 将接口转换成路由
     *
     * @param interfaceEntity
     * @return
     * @throws URISyntaxException
     */
    RouteDefinition interfaceEntityToRouteDefinition(ServiceInterfaceEntity interfaceEntity) throws URISyntaxException;

    /**
     * 将路由转换成服务实体
     *
     * @param routeDefinition
     * @return
     */
    ServiceInterfaceEntity routeDefinitionToInterfaceEntity(RouteDefinition routeDefinition);
}
