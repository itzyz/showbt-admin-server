package com.showbt.cloud.gateway.util;

import com.showbt.cloud.common.core.util.TransUtils;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * 验证码转换类
 *
 * @author loris
 */
public class SigUtils {

    public static String encrypt(Map<String, String> map, String apikey) throws UnsupportedEncodingException {
        String str = TransUtils.transMapToString(map);
        return getMD5(URLDecoder.decode(str, "utf-8") + apikey);
    }

    private static String getMD5(String plainText) {
        byte[] secretBytes = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            secretBytes = md.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有md5这个算法");
        }

        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
}
