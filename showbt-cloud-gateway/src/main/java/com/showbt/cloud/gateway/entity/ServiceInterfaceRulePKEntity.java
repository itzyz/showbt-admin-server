package com.showbt.cloud.gateway.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ServiceInterfaceRulePKEntity implements Serializable {
    @ManyToOne
    @JoinColumn(name = "server_id")
    @JsonIgnoreProperties("configs")
    private ServiceInterfaceEntity server;
    @ManyToOne
    @JoinColumn(name = "rule_id")
    private RuleDictEntity ruleDict;
}
