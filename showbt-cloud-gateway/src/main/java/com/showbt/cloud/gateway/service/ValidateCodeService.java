package com.showbt.cloud.gateway.service;

import com.alibaba.fastjson.JSONObject;
import com.showbt.cloud.common.core.util.R;

/**
 * 处理分布式验证码
 */
public interface ValidateCodeService {
    /**
     * 生成验证码
     * @return
     */
    public R<JSONObject> generateValidateCode();

    /**
     * 检查验证码是否正确，正确返回true
     * @param uuid
     * @param code
     * @return
     */
    public R checkValidateCode(String uuid, String code);
}
