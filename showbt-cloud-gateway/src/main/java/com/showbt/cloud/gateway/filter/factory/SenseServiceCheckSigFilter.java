package com.showbt.cloud.gateway.filter.factory;

import com.alibaba.fastjson.JSONObject;
import com.showbt.cloud.gateway.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.support.BodyInserterContext;
import org.springframework.cloud.gateway.support.CachedBodyOutputMessage;
import org.springframework.cloud.gateway.support.DefaultServerRequest;
import org.springframework.cloud.gateway.support.ipresolver.XForwardedRemoteAddressResolver;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
public class SenseServiceCheckSigFilter extends AbstractGatewayFilterFactory {

    @Autowired
    private AuthService authService;

    @Override
    @SuppressWarnings("unchecked")
    public GatewayFilter apply(Object config) {

        return (exchange, chain) -> {
            ServerRequest serverRequest = new DefaultServerRequest(exchange);

            HttpHeaders headers = new HttpHeaders();
            headers.putAll(exchange.getRequest().getHeaders());

            InetSocketAddress isa = XForwardedRemoteAddressResolver.trustAll().resolve(exchange);

            log.info("access info   " + "   " + headers.get("X-Real-IP") + "  " + isa.getAddress() + "  "
                    + exchange.getRequest().getRemoteAddress() + " " + exchange.getRequest().getPath()
                    + "    " + exchange.getRequest().getQueryParams());

            Mono<String> modifiedBody = serverRequest.bodyToMono(String.class)
                    .flatMap(body -> {
                        String decode = null;
                        try {
                            decode = URLDecoder.decode(body, "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        if (StringUtils.isNotEmpty(decode)) {
                            Map<String, Object> bodyMap = resolveBodyToMap(decode);
                            //验签
                            if (isChecked(bodyMap)) {
                                //todo 此处应该有日志，记录请求APPid，IP地址，时间，任务ID
                                log.info("请求成功：" + JSONObject.toJSONString(bodyMap));
                                headers.add("authCode", "success");
                                headers.add("authMsg", "请求成功");
                                return Mono.just(body);
                            }
                            log.info("请求失败，请求未通过授权！" + JSONObject.toJSONString(bodyMap));
                            headers.add("authCode", "fail");
                            headers.add("authMsg", "请求失败，请求未通过授权！");
                            return Mono.empty();
                        }
                        return Mono.empty();
                    });

            BodyInserter bodyInserter = BodyInserters.fromPublisher(modifiedBody, String.class);

            headers.remove(HttpHeaders.CONTENT_LENGTH);

            CachedBodyOutputMessage outputMessage = new CachedBodyOutputMessage(exchange, headers);
            return bodyInserter.insert(outputMessage, new BodyInserterContext())
                    .then(Mono.defer(() -> {
                        ServerHttpRequestDecorator decorator = new ServerHttpRequestDecorator(
                                exchange.getRequest()) {
                            @Override
                            public HttpHeaders getHeaders() {
                                long contentLength = headers.getContentLength();
                                HttpHeaders httpHeaders = new HttpHeaders();
                                httpHeaders.putAll(super.getHeaders());
                                httpHeaders.set("authCode", headers.getFirst("authCode"));
                                httpHeaders.set("authMsg", headers.getFirst("authMsg"));
                                if (contentLength > 0) {
                                    httpHeaders.setContentLength(contentLength);
                                } else {
                                    httpHeaders.set(HttpHeaders.TRANSFER_ENCODING, "chunked");
                                }
                                return httpHeaders;
                            }

                            @Override
                            public Flux<DataBuffer> getBody() {
                                return outputMessage.getBody();
                            }
                        };
                        String authCode = decorator.getHeaders().getFirst("authCode");
                        decorator.getHeaders().remove("authCode");
                        if (null != authCode && authCode.equals("success")) {
                            return chain.filter(exchange.mutate().request(decorator).build());
                        } else {
                            ServerHttpResponse response = exchange.getResponse();
                            response.setStatusCode(HttpStatus.OK);
                            JSONObject msg = new JSONObject();
                            String authMsg = decorator.getHeaders().getFirst("authMsg");
                            decorator.getHeaders().remove("authMsg");
                            msg.put("msg", authMsg != null ? authMsg : "请求失败，请求参数为空！");
                            msg.put("code", "402");
                            msg.put("data", Collections.emptyList());
                            byte[] bytes = msg.toJSONString().getBytes(StandardCharsets.UTF_8);
                            DataBuffer buffer = response.bufferFactory().wrap(bytes);
                            response.getHeaders().add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
                            return response.writeWith(Mono.just(buffer));
                        }
                    }));
        };
    }

    private Map<String, Object> resolveBodyToMap(String body) {
        Map<String, Object> map = new HashMap<>();
        String[] bodySplit = body.split("&");
        for (String param : bodySplit) {
            String key = param.split("=")[0];
            String value = param.split("=")[1];
            if ("param".equals(key)) {
                value = param.substring(param.indexOf("param=") + 6);
            }
            map.put(key, value);
        }
        return map;
    }

    private boolean isChecked(Map<String, Object> params) {
//        R<Boolean> r = authService.checkSig(params);
//        return r.getData();
        return false;
    }
}


