package com.showbt.cloud.gateway.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.showbt.project.base.model.BaseEntity;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * 服务接口实体对象
 */
@Entity
@Table(name = "cpt_server",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"label", "m_id"})},
        indexes = {@Index(columnList = "label")})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ServiceInterfaceEntity extends BaseEntity<String> {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    /**
     * 服务名称
     */
    @Column(length = 10)
    private String serverName;
    /**
     * 服务请求虚拟目录
     */
    @Column(length = 32)
    private String label;
    /**
     * 服务真实地址，相对路径
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false, columnDefinition = "Text")
    private String realPath;
    /**
     * 项目模块
     */
    @ManyToOne
    @JoinColumn(name = "m_id")
    @JsonIgnoreProperties("servers")
    private ModuleEntity module;
    /**
     * 服务过滤规则和转发规则
     */
    @OneToMany(mappedBy = "srpk.server", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnoreProperties("srpk.server")
    private List<ServiceInterfaceRuleEntity> configs;

    @Column(name = "fixed_params")
    private String fixedParams;
}
