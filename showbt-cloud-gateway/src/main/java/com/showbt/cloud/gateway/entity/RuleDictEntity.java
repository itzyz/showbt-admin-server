package com.showbt.cloud.gateway.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "cpt_rule_dict",
        indexes = {@Index(columnList = "ruleType")})
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RuleDictEntity {
    @Id
    @GenericGenerator(name = "jpa-uuid", strategy = "uuid")
    @GeneratedValue(generator = "jpa-uuid")
    @Column(length = 32)
    private String id;
    @Column(length = 100)
    private String ruleName;
    /**
     * 规则类型，对应filter, predicate等
     */
    @Column(length = 20)
    private String ruleType;
    /**
     * 规则对应的参数
     */
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false, columnDefinition = "Text")
    private String args;
}
