package com.showbt.cloud.gateway.filter.factory;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 密码解密工具类
 */
@Slf4j
@Component
public class PasswordDecoderFilter extends AbstractGatewayFilterFactory {
    private static final String PASSWORD = "password";
    private static final String KEY_ALGORITHM = "AES";
    @Value("${security.encode.key:1234567812345678}")
    private String encodeKey;

    private static String decryptAES(String data, String pass) {
        AES aes = new AES(Mode.CBC, Padding.NoPadding,
                new SecretKeySpec(pass.getBytes(), KEY_ALGORITHM),
                new IvParameterSpec(pass.getBytes()));
        byte[] result = aes.decrypt(Base64.decode(data.getBytes(StandardCharsets.UTF_8)));
        return new String(result, StandardCharsets.UTF_8);
    }

    @Override
    public GatewayFilter apply(Object config) {
        return (exchange, chain) -> {
            Mono<Void> voidMono = handlerParamter(exchange, chain);
            return voidMono;
        };
    }

    private Mono<Void> handlerParamter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest serverHttpRequest = exchange.getRequest();
        HttpMethod method = serverHttpRequest.getMethod();
        String contentType = serverHttpRequest.getHeaders().getFirst(HttpHeaders.CONTENT_TYPE);
        ShowbtServerHttpRequestDecorder requestDecorder = new ShowbtServerHttpRequestDecorder();
        if (method == HttpMethod.POST
                && (MediaType.APPLICATION_FORM_URLENCODED_VALUE.equalsIgnoreCase(contentType)
                || MediaType.APPLICATION_JSON_VALUE.equalsIgnoreCase(contentType))) {
            //从post请求中获取请求体
            String bodyStr = requestDecorder.resolveBodyFromRequest(serverHttpRequest);
            //请求参数为空时，直接结束请求
            if (StringUtils.isBlank(bodyStr)) {
                log.error("请求异常：{} 请求参数不能为空", serverHttpRequest.getURI().getRawPath());
                ServerHttpResponse serverHttpResponse = exchange.getResponse();
                serverHttpResponse.setStatusCode(HttpStatus.BAD_REQUEST);
                return serverHttpResponse.setComplete();
            }
            String newBodyStr = "";
            //表单请求参数处理
            if (MediaType.APPLICATION_FORM_URLENCODED_VALUE.equalsIgnoreCase(contentType)) {
                Map<String, String> paramMap = HttpUtil.decodeParamMap(bodyStr, CharsetUtil.UTF_8);
                try {
                    paramMap = decodePassword(paramMap);
                } catch (Exception e) {
                    return Mono.error(e);
                }
                newBodyStr = HttpUtil.toParams(paramMap);
            }
            //JSON参数解析
            if (MediaType.APPLICATION_JSON_VALUE.equalsIgnoreCase(contentType) || MediaType.APPLICATION_JSON_UTF8_VALUE.equalsIgnoreCase(contentType)) {
                JSONObject json = JSON.parseObject(bodyStr);
                String password = json.getString(PASSWORD);
                try {
                    json.put(PASSWORD, decryptAES(password, encodeKey).trim());
                } catch (Exception e) {
                    log.error("密码解密失败:{}", password);
                    return Mono.error(e);
                }
                newBodyStr = json.toJSONString();
            }
            ServerHttpRequest newServerHttpRequest = requestDecorder.createPostServerHttpRequest(exchange, newBodyStr);
            return chain.filter(exchange.mutate().request(newServerHttpRequest).build());
        } else if (method == HttpMethod.GET) {
            URI uri = serverHttpRequest.getURI();
            Map<String, String> paramMap = serverHttpRequest.getQueryParams().toSingleValueMap();
            String password = paramMap.get(PASSWORD);
            try {
                paramMap.put(PASSWORD, decryptAES(password, encodeKey).trim());
            } catch (Exception e) {
                log.error("密码解密失败:{}", password);
                return Mono.error(e);
            }
            String newParamStr = HttpUtil.toParams(paramMap);

            // 替换查询参数
            URI newUri = UriComponentsBuilder.fromUri(uri).replaceQuery(newParamStr).build(true).toUri();
            ServerHttpRequest newRequest = exchange.getRequest().mutate().uri(newUri).build();
            return chain.filter(exchange.mutate().request(newRequest).build());
        }
        return chain.filter(exchange);
    }

    /**
     * 密码解密
     *
     * @param paramMap
     * @return
     * @throws Exception
     */
    private Map<String, String> decodePassword(Map<String, String> paramMap) throws Exception {
        String password = paramMap.get(PASSWORD);
        if (StrUtil.isNotBlank(password)) {
            try {
                password = decryptAES(password, encodeKey);
            } catch (Exception e) {
                log.error("密码解密失败:{}", password);
                throw new Exception("密码解密失败");
            }
            paramMap.put(PASSWORD, password.trim());
        }
        return paramMap;
    }

    /*private String resolveBodyFromRequest(ServerHttpRequest serverHttpRequest) {
        //获取请求体
        Flux<DataBuffer> body = serverHttpRequest.getBody();
        AtomicReference<String> bodyRef = new AtomicReference<>();

        body.subscribe(buffer -> {
            CharBuffer charBuffer = StandardCharsets.UTF_8.decode(buffer.asByteBuffer());
            DataBufferUtils.release(buffer);
            bodyRef.set(charBuffer.toString());
        });

        return bodyRef.get();
    }*/

    /**
     * 将字符串转换成DataBuffer
     *
     * @param body
     * @return
     */
/*    private DataBuffer stringToBuffer(String body) {
        byte[] bytes = body.getBytes(StandardCharsets.UTF_8);
        NettyDataBufferFactory nettyDataBufferFactory = new NettyDataBufferFactory(ByteBufAllocator.DEFAULT);
        DataBuffer buffer = nettyDataBufferFactory.allocateBuffer(bytes.length);
        buffer.write(bytes);
        return buffer;
    }*/
}
