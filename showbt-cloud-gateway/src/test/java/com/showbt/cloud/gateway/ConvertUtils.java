package com.showbt.cloud.gateway;

import org.xml.sax.SAXException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.sax.TransformerHandler;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ConvertUtils {

    public static String convertToXML(Object obj) throws Exception {
        Writer writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);

        SAXTransformerFactory tf = (SAXTransformerFactory) SAXTransformerFactory.newInstance();
        try {
            TransformerHandler tfh = tf.newTransformerHandler();
            Transformer serializer = tfh.getTransformer();

            serializer.setOutputProperty("encoding", "UTF-8");
            tfh.setResult(streamResult);

            tfh.startDocument();
            tfh.startElement("", "", "contents", null);
            tfh.startElement("", "", "content", null);

            toXMLInternal(tfh, obj);

            tfh.endElement("", "", "content");
            tfh.endElement("", "", "contents");
            tfh.endDocument();
        } catch (TransformerConfigurationException e) {
            throw new Exception("-2" + "cannot find xml parser:" + e.getMessage());
        } catch (SAXException e) {
            throw new Exception(-2 + "xml parser error:" + e.getMessage());
        }

        return writer.toString();
    }

    private static void toXMLInternal(TransformerHandler tfh, Object obj) throws SAXException {
        Field[] fields = obj.getClass().getFields();

        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (!Modifier.isStatic(field.getModifiers())) {
                String fieldName = field.getName();

                if (isIgnoreField(fieldName)) {
                    continue;
                }
                if (fieldName.equals("classId")) {
                    fieldName = "class";
                }
                tfh.startElement("", "", fieldName, null);

                char[] ch = null;
                try {
                    ch = field.get(obj).toString().toCharArray();

                    if ((fieldName.equals("title")) || (fieldName.equals("text"))) {
                        replaceChars(ch);
                    }

                } catch (Exception localException) {
                }

                if (ch != null) {
                    tfh.startCDATA();
                    tfh.characters(ch, 0, ch.length);
                    tfh.endCDATA();
                }

                tfh.endElement("", "", fieldName);
            }
        }
    }

    private static boolean isIgnoreField(String name) {
        return name.equals("version") || name.equals("format")
                || name.equals("flag") || name.equals("taskId")
                || name.equals("transId") || name.equals("interface_url")
                || name.equals("time") || name.equals("apikey");
    }

    private static char[] replaceChars(char[] ch) {
        for (int i = 0; i < ch.length; i++) {
            int tmpch = ch[i];
            if ((tmpch < 32) && (tmpch != 9) && (tmpch != 10) && (tmpch != 13)) {
                ch[i] = ' ';
            }
        }
        return ch;
    }
}
