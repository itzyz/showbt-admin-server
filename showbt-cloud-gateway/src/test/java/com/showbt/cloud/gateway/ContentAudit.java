package com.showbt.cloud.gateway;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContentAudit {
    /**
     * param 参数
     */
    public String classId;
    public String textId;
    public String url;
    public String title;
    public String text;
    public String author;
    public String userId;
    public String ip;
    public String pubDate;
    public String threadId;
    public String authorEx;
    public String contentEx;
    public String structureEx;
    public String rules;

    public String flag;

    /**
     * 公共参数
     */
    public String interface_url;
    public String taskId;
    public String transId;
    public long time;
    public String format;
    public String version;
    public String apikey;
}
