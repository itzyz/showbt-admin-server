package com.showbt.cloud.gateway;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@UtilityClass
public class AuditClient {
    public String audit(ContentAudit audit) {
        CloseableHttpClient client = null;
        try {
            client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(audit.getInterface_url());
            List<NameValuePair> body = settingBody(audit);
            httpPost.setEntity(new UrlEncodedFormEntity(body, StandardCharsets.UTF_8));
            CloseableHttpResponse response = client.execute(httpPost);
            return convertResponseToStr(response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (client != null) {
                    client.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private String convertResponseToStr(CloseableHttpResponse response) {
        if (response != null) {
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
                    StringBuilder sb = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                    return sb.toString();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    private List<NameValuePair> settingBody(ContentAudit audit) {
        List<NameValuePair> body = new ArrayList<>();
        body.add(new BasicNameValuePair("taskId", audit.getTaskId()));
        body.add(new BasicNameValuePair("time", String.valueOf(audit.getTime())));
        body.add(new BasicNameValuePair("sig", getSig(audit)));
        body.add(new BasicNameValuePair("transId", audit.getTransId()));
        try {
            body.add(new BasicNameValuePair("param", ConvertUtils.convertToXML(audit)));
        } catch (Exception e) {
            System.out.println("param转化错误");
            e.printStackTrace();
        }
        if (StringUtils.isNotEmpty(audit.getFormat())) {
            body.add(new BasicNameValuePair("format", audit.getFormat()));
        }
        if (StringUtils.isNotEmpty(audit.getVersion())) {
            body.add(new BasicNameValuePair("version", audit.getVersion()));
        }
        return body;
    }

    private String getSig(ContentAudit audit) {

        Map<String, String> sigMap = new HashMap<>();

        sigMap.put("time", String.valueOf(audit.getTime()));
        sigMap.put("taskId", audit.getTaskId());
        sigMap.put("transId", audit.getTransId());
        if (StringUtils.isNotEmpty(audit.getFormat())) {
            sigMap.put("format", audit.getFormat());
        }
        if (StringUtils.isNotEmpty(audit.getVersion())) {
            sigMap.put("version", audit.getVersion());
        }

        try {
            sigMap.put("param", ConvertUtils.convertToXML(audit));
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> list = new ArrayList<>(sigMap.keySet());
        Collections.sort(list);

        StringBuilder sb = new StringBuilder();

        Iterator<String> iterator = list.iterator();

        while (iterator.hasNext()) {
            String key = iterator.next();
            sb.append(key).append("=").append(sigMap.get(key));
            if (iterator.hasNext()) {
                sb.append("&");
            }
        }

        sb.append(audit.getApikey());
        return getMD5(sb.toString());
    }

    public String getMD5(String plainText) {
        byte[] secretBytes = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            secretBytes = md.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有md5这个算法");
        }

        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
}
