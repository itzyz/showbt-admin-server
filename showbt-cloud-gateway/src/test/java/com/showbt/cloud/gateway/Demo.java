package com.showbt.cloud.gateway;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@SpringBootTest
public class Demo {

    //    private final static String INTERFACE_URL = "http://10.125.9.71:8081/xh/contentAudit/predict";
    private final static String INTERFACE_URL = "http://10.125.9.71:9000/audit/content/long_text";
//    private final static String INTERFACE_URL = "http://localhost:9000/audit/content/long_text";

    /**
     * 测试任务
     */
    @Test
    public void testConentAudit() {

        String text = "习进平国际金融机构负责人“求解”中国移动支付领先世界秘诀";

        ContentAudit audit = getContentAudit();
        audit.setTaskId("0918d11f5d454c39a0394f17cbf2bb26");
        audit.setApikey("87642c989b5c4667bcf0d4431458ac8b");
        audit.setText(text);

        printResult(audit);
    }

    /**
     * 创建对象属性
     *
     * @return
     */
    private ContentAudit getContentAudit() {
        return ContentAudit.builder()
                .transId(UUID.randomUUID().toString().replace("-", ""))
                .time(new Date().getTime() / 1000L)
                .classId("1")
                .textId("1")
                .url("")
                .userId("1")
                .title("")
                .pubDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))
                .format("JSON")
                .version("2.0")
                .interface_url(INTERFACE_URL)
                .build();
    }

    /**
     * 打印结果
     *
     * @param audit
     */
    private void printResult(ContentAudit audit) {
        long star = System.currentTimeMillis();
        String result = AuditClient.audit(audit);
        long end = System.currentTimeMillis();
        System.out.println(end - star);
        System.out.println(JSON.toJSONString(JSON.parseObject(result), true));
    }
}
