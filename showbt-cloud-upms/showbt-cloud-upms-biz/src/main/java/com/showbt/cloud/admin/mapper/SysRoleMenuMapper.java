package com.showbt.cloud.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.showbt.cloud.admin.api.entity.SysRoleMenu;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
