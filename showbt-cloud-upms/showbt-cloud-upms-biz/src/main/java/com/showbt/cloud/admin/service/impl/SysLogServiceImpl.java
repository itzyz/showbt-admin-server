package com.showbt.cloud.admin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.showbt.cloud.admin.api.entity.SysLog;
import com.showbt.cloud.admin.mapper.SysLogMapper;
import com.showbt.cloud.admin.service.SysLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 日志表 服务实现类
 * </p>
 */
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog> implements SysLogService {

}
