package com.showbt.cloud.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.showbt.cloud.admin.api.entity.SysDict;

/**
 * <p>
 * 字典表 服务类
 * </p>
 */
public interface SysDictService extends IService<SysDict> {
}
