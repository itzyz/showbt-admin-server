package com.showbt.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.showbt.cloud.admin.api.entity.SysLog;

/**
 * <p>
 * 日志表 Mapper 接口
 * </p>
 */
public interface SysLogMapper extends BaseMapper<SysLog> {
}
