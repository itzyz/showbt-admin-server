package com.showbt.cloud.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.showbt.cloud.admin.api.entity.SysLog;

/**
 * <p>
 * 日志表 服务类
 * </p>
 */
public interface SysLogService extends IService<SysLog> {

}
