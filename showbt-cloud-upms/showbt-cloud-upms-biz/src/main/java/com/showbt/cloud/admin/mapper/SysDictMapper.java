package com.showbt.cloud.admin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.showbt.cloud.admin.api.entity.SysDict;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
