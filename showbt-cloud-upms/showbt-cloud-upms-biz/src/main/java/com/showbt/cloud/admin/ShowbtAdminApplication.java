package com.showbt.cloud.admin;


import com.showbt.cloud.common.security.annotation.EnableShowbtFeignClients;
import com.showbt.cloud.common.security.annotation.EnableShowbtResourceServer;
import org.springframework.boot.SpringApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * 统一用户管理系统
 */
@EnableCaching
@EnableShowbtResourceServer
@EnableShowbtFeignClients
@SpringCloudApplication
public class ShowbtAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShowbtAdminApplication.class, args);
    }

}
