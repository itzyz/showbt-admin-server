package com.showbt.cloud.admin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.showbt.cloud.admin.api.entity.SysOauthClientDetails;

/**
 * <p>
 * Mapper 接口
 * </p>
 */
public interface SysOauthClientDetailsMapper extends BaseMapper<SysOauthClientDetails> {

}
