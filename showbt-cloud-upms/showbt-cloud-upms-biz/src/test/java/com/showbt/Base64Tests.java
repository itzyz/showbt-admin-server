package com.showbt;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Base64Tests {

    @Test
    public void test() throws UnsupportedEncodingException {
        final Base64.Decoder decoder = Base64.getDecoder();
        final Base64.Encoder encoder = Base64.getEncoder();
        final String text = "showbt:showbt";
        final byte[] textByte = text.getBytes("UTF-8");
//编码
        final String encodedText = encoder.encodeToString(textByte);
        System.out.println(encodedText);
//解码
        System.out.println(new String(decoder.decode("c2hvd2J0OnNob3didA=="), "UTF-8"));

    }

    @Test
    public void test1(){
        String content = "123456";

//随机生成密钥
        byte[] key = "showbt-cloud:xxx".getBytes();

//构建
        AES aes = SecureUtil.aes(key);

//加密
        byte[] encrypt = aes.encrypt(content);
        System.out.println(encrypt);
//解密
        byte[] decrypt = aes.decrypt(encrypt);
        System.out.println(new String(decrypt));
    }
}
