package com.showbt;

import org.jasypt.util.text.BasicTextEncryptor;
import org.junit.Test;

import java.util.Base64;

public class EncrypterTests {
    @Test
    public void jasyptTest() {
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        // application.properties, jasypt.encryptor.password
//        encryptor.setPassword("showbt");
        // encrypt root
//        System.out.println(encryptor.encrypt("showbt-api"));
//        System.out.println(encryptor.encrypt("showbt-api"));
//        System.out.println(encryptor.encrypt("cloud"));

//        ENC(JuCbQwzUjqI0x/al9zjJbQ==)
//        ENC(bYf2s0HIjba2EVTtCUTcxA==)

        // decrypt, the result is root
//        System.out.println(encryptor.decrypt("q4Wc01ddrrj64Kwn3PevEQ=="));
//        System.out.println(encryptor.decrypt("gMKP6n3LGDvyxUv+SFssvQ=="));


        encryptor.setPassword("showbt.com,wwwww");
        System.out.println(encryptor.encrypt("showbt"));
    }
}