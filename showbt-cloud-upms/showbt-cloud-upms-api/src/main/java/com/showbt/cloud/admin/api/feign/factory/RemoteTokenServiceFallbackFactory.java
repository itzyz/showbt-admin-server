package com.showbt.cloud.admin.api.feign.factory;

import com.showbt.cloud.admin.api.feign.RemoteTokenService;
import com.showbt.cloud.admin.api.feign.fallback.RemoteTokenServiceFallbackImpl;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class RemoteTokenServiceFallbackFactory implements FallbackFactory<RemoteTokenService> {

    @Override
    public RemoteTokenService create(Throwable throwable) {
        RemoteTokenServiceFallbackImpl remoteTokenServiceFallback = new RemoteTokenServiceFallbackImpl();
        remoteTokenServiceFallback.setCause(throwable);
        return remoteTokenServiceFallback;
    }
}
