package com.showbt.registry.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author Loris
 * @date Created in 2020/6/28 下午4:48
 * @since restTemplate配置
 */
@Configuration
public class RestConfig {

    @Value("${xh.restTemplate.connect-timeout}")
    private int connectTimeout;
    @Value("${xh.restTemplate.read-timeout}")
    private int readTimeout;

    @Bean
    public RestTemplate restTemplate() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(connectTimeout);
        factory.setReadTimeout(readTimeout);
        return new RestTemplate(factory);
    }
}