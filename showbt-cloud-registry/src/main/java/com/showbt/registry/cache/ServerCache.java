package com.showbt.registry.cache;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Loris
 * @date Created in 2020/6/29 下午3:50
 * @since
 */
public interface ServerCache {
    Map<String, List<String>> SERVER_MAP = new HashMap<>();
}
