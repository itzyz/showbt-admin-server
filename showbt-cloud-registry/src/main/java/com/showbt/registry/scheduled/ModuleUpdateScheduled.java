package com.showbt.registry.scheduled;

import com.showbt.registry.cache.ServerCache;
import com.showbt.registry.service.CustomNacosNamingService;
import com.showbt.registry.service.ServerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Loris
 * @date Created in 2020/6/29 下午3:38
 * @since 服务更新定时任务
 */
@Slf4j
@Component
public class ModuleUpdateScheduled {

    @Autowired
    private ServerService serverService;
    @Autowired
    private CustomNacosNamingService namingService;

    @Scheduled(cron = "${xh.scheduled.cron}")
    public void moduleUpdate() {

        Map<String, List<String>> serverMap = serverService.findServers();
        if (ServerCache.SERVER_MAP.equals(serverMap)) {
            log.info("======= No Change =======");
            return;
        }

        Map<String, List<String>> persistMap = new HashMap<>();
        Map<String, List<String>> removeMap = new HashMap<>();
        serverMap.forEach((serviceName, ips) -> {
            if (!ServerCache.SERVER_MAP.containsKey(serviceName)) {
                persistMap.put(serviceName, ips);
                return;
            }
            List<String> ipCache = ServerCache.SERVER_MAP.get(serviceName);

            List<String> tempList = new ArrayList<>(ips);

            if (!ipCache.equals(ips)) {
                ServerCache.SERVER_MAP.put(serviceName, ips);
                //交集
                tempList.retainAll(ipCache);
                //将更新后,去掉的实例的心跳停止
                ipCache.removeAll(tempList);
                if (ipCache.size() > 0) {
                    removeMap.put(serviceName, ipCache);
                }
                //新增实例的心跳
                ips.removeAll(tempList);
                if (ips.size() > 0) {
                    persistMap.put(serviceName, ips);
                }
            }
        });

        if (!persistMap.isEmpty()) {
            namingService.registerInstances(persistMap);
        }
        if (!removeMap.isEmpty()) {
            namingService.removeInstances(removeMap);
        }

        log.info("====== Server Update, Persist: {}, Remove: {} ======", persistMap.size(), removeMap.size());
    }
}
