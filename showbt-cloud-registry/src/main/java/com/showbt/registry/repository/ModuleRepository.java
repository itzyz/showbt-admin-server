package com.showbt.registry.repository;

import com.showbt.registry.entity.Module;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Loris
 * @date Created in 2020/6/28 下午3:49
 * @since
 */
public interface ModuleRepository extends JpaRepository<Module, String> {
}
