package com.showbt.registry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class ShowbtRegistryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShowbtRegistryApplication.class, args);
    }

}
