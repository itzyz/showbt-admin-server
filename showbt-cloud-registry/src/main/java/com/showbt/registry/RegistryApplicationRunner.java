package com.showbt.registry;

import com.showbt.registry.cache.ServerCache;
import com.showbt.registry.service.CustomNacosNamingService;
import com.showbt.registry.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * @author Loris
 * @date Created in 2020/6/29 下午1:14
 * @since 项目启动注册
 */
@Component
public class RegistryApplicationRunner implements ApplicationRunner {

    @Autowired
    private CustomNacosNamingService namingService;
    @Autowired
    private ServerService serverService;

    @Override
    public void run(ApplicationArguments args) {
        Map<String, List<String>> serverMap = serverService.findServers();
        ServerCache.SERVER_MAP.putAll(serverMap);
        namingService.registerInstances(serverMap);
    }
}
