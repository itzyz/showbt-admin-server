package com.showbt.registry.service;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.common.Constants;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.alibaba.nacos.api.naming.utils.NamingUtils;
import com.alibaba.nacos.client.naming.beat.BeatInfo;
import com.alibaba.nacos.client.naming.net.NamingProxy;
import com.alibaba.nacos.client.naming.utils.InitUtils;
import com.alibaba.nacos.client.naming.utils.UtilAndComs;
import com.showbt.registry.beat.CustomBeatReactor;
import com.showbt.registry.constant.NacosConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author Loris
 * @date Created in 2020/6/28 下午3:10
 * @since 自定义心跳检查
 */
@Slf4j
@Component
public class CustomNacosNamingService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private NacosDiscoveryProperties nacosProperties;

    private NamingProxy serverProxy;

    private CustomBeatReactor beatReactor;

    private void init(Properties properties) {
        String namespace = InitUtils.initNamespaceForNaming(properties);
        String endpoint = InitUtils.initEndpoint(properties);
        String serverList = properties.getProperty(PropertyKeyConst.SERVER_ADDR);
        serverProxy = new NamingProxy(namespace, endpoint, serverList, properties);
        beatReactor = new CustomBeatReactor(restTemplate, serverProxy, initClientBeatThreadCount(properties));
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.SERVER_ADDR, nacosProperties.getServerAddr());
        return properties;
    }

    private int initClientBeatThreadCount(Properties properties) {
        if (properties == null) {
            return UtilAndComs.DEFAULT_CLIENT_BEAT_THREAD_COUNT;
        }

        return NumberUtils.toInt(properties.getProperty(PropertyKeyConst.NAMING_CLIENT_BEAT_THREAD_COUNT),
                UtilAndComs.DEFAULT_CLIENT_BEAT_THREAD_COUNT);
    }

    public void registerInstances(Map<String, List<String>> serviceMap) {
        Properties properties = getProperties();
        init(properties);
        serviceMap.forEach((serviceName,ips) -> {
            ips.forEach(ip -> {
                try {
                    //run beatTask and registry
                    Instance instance = new Instance();
                    //ip: host:port/health
                    String host = ip.split(":")[0];
                    int port = ip.contains(":") ? Integer.parseInt(ip.contains("/") ? ip.split(":")[1].substring(0, ip.split(":")[1].indexOf("/")) : ip.split(":")[1]) : 80;
                    instance.setIp(host);
                    instance.setPort(port);
                    instance.setWeight(1.0);
                    instance.setClusterName(Constants.DEFAULT_CLUSTER_NAME);
                    if (instance.isEphemeral()) {
                        BeatInfo beatInfo = new BeatInfo();
                        beatInfo.setServiceName(NamingUtils.getGroupedName(serviceName, Constants.DEFAULT_GROUP));
                        beatInfo.setIp(instance.getIp());
                        beatInfo.setPort(instance.getPort());
                        beatInfo.setCluster(instance.getClusterName());
                        beatInfo.setWeight(instance.getWeight());
                        beatInfo.setMetadata(instance.getMetadata());
                        beatInfo.setScheduled(false);
                        beatInfo.setPeriod(instance.getInstanceHeartBeatInterval());

                        beatInfo.getMetadata().put(NacosConstants.HEALTH_PATH, "http://" + ip);

                        beatReactor.addBeatInfo(NamingUtils.getGroupedName(serviceName, Constants.DEFAULT_GROUP), beatInfo);
                    }
                    ResponseEntity<String> response = restTemplate.getForEntity("http://" + ip, String.class);
                    if (HttpStatus.OK.equals(response.getStatusCode())) {
                        serverProxy.registerService(NamingUtils.getGroupedName(serviceName, Constants.DEFAULT_GROUP), Constants.DEFAULT_GROUP, instance);
                    }
                } catch (Exception e) {
                    log.error("====== Registry Error, service_name: {}, ip: {}, Message: {} ======", serviceName, ip, e.getMessage());
                }
            });
        });
    }

    public void removeInstances(Map<String, List<String>> serviceMap) {
        if (serverProxy == null || beatReactor == null) {
            Properties properties = getProperties();
            init(properties);
        }
        serviceMap.forEach((serviceName,ips) -> {
            ips.forEach(ip -> {
                String host = ip.split(":")[0];
                int port = ip.contains(":") ? Integer.parseInt(ip.contains("/") ? ip.split(":")[1].substring(0, ip.split(":")[1].indexOf("/")) : ip.split(":")[1]) : 80;
                beatReactor.removeBeatInfo(NamingUtils.getGroupedName(serviceName, Constants.DEFAULT_GROUP), host, port);
            });
        });
    }
}
