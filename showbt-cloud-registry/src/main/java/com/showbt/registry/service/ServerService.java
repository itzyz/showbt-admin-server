package com.showbt.registry.service;

import java.util.List;
import java.util.Map;

/**
 * @author Loris
 * @date Created in 2020/6/29 下午3:48
 * @since 查找所有的项目信息
 */
public interface ServerService {
    /**
     * 将所有的模块数据转化为{serviceName: [ips]}
     *
     * @return {serviceName: [ips]}
     */
    Map<String, List<String>> findServers();
}
