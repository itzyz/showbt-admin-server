package com.showbt.registry.service;

import com.showbt.registry.entity.Module;
import com.showbt.registry.repository.ModuleRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Loris
 * @date Created in 2020/6/29 下午3:47
 * @since
 */
@Service
public class ServerServiceImpl implements ServerService {
    @Autowired
    private ModuleRepository moduleRepository;

    @Override
    public Map<String, List<String>> findServers() {
        List<Module> modules = moduleRepository.findAll();
        //modules => {serviceName: [ips]}
        return modules.stream()
                .filter(module -> !module.getRealPath().startsWith("lb://"))
                .collect(Collectors.toMap(
                        module -> module.getProject().getLabel().replace("/", "") + module.getLabel().replace("/", "-"),
                        module -> {
                            String realPath = module.getRealPath().replaceAll("http://", "");
                            String health = module.getHealth();
                            return Arrays.stream(realPath.split(","))
                                    .filter(StringUtils::isNotEmpty)
                                    .map(ip -> {
                                        if (StringUtils.isNotEmpty(health)) {
                                            return ip + health;
                                        }
                                        return ip;
                                    })
                                    .collect(Collectors.toList());
                        }));
    }
}
