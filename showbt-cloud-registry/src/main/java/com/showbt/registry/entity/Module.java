package com.showbt.registry.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * @author Loris
 * @date Created in 2020/6/28 下午3:45
 * @since 模块
 */
@Entity
@Table(name = "cpt_module")
@Setter
@Getter
@ToString
public class Module {
    @Id
    private String id;
    /** 标识 */
    private String label;
    /** 真实地址 */
    private String realPath;
    /** 心跳地址 */
    private String health;
    /** 所属项目 */
    @ManyToOne
    @JoinColumn(name = "p_id")
    private Project project;
}
