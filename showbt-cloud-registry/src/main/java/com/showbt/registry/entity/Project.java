package com.showbt.registry.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Loris
 * @date Created in 2020/6/28 下午3:45
 * @since 项目
 */
@Entity
@Table(name = "cpt_project")
@Setter
@Getter
@ToString
public class Project {
    @Id
    private String id;
    /** 标识 */
    private String label;
}
