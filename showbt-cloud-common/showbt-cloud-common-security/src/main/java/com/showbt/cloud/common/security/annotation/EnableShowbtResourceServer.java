package com.showbt.cloud.common.security.annotation;

import com.showbt.cloud.common.security.component.ShowbtResourceServerAutoConfiguration;
import com.showbt.cloud.common.security.component.ShowbtSecurityBeanDefinitionRegistrar;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import java.lang.annotation.*;

/**
 * 资源服务注解
 */
@Documented
@Inherited
@EnableResourceServer
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Import({ShowbtResourceServerAutoConfiguration.class, ShowbtSecurityBeanDefinitionRegistrar.class})
public @interface EnableShowbtResourceServer {

}
