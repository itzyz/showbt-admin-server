package com.showbt.cloud.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.showbt.cloud.common.security.component.ShowbtAuth2ExceptionSerializer;
import lombok.Getter;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * 自定义OAuth2Exception
 */
@JsonSerialize(using = ShowbtAuth2ExceptionSerializer.class)
public class ShowbtAuth2Exception extends OAuth2Exception {
    @Getter
    private String errorCode;

    public ShowbtAuth2Exception(String msg) {
        super(msg);
    }

    public ShowbtAuth2Exception(String msg, String errorCode) {
        super(msg);
        this.errorCode = errorCode;
    }
}
