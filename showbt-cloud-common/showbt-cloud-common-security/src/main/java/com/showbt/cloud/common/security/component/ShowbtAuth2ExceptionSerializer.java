package com.showbt.cloud.common.security.component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.showbt.cloud.common.core.constant.CommonConstants;
import com.showbt.cloud.common.security.exception.ShowbtAuth2Exception;
import lombok.SneakyThrows;

/**
 * OAuth2 异常格式化
 */
public class ShowbtAuth2ExceptionSerializer extends StdSerializer<ShowbtAuth2Exception> {
    public ShowbtAuth2ExceptionSerializer() {
        super(ShowbtAuth2Exception.class);
    }

    @Override
    @SneakyThrows
    public void serialize(ShowbtAuth2Exception value, JsonGenerator gen, SerializerProvider provider) {
        gen.writeStartObject();
        gen.writeObjectField("code", CommonConstants.FAIL);
        gen.writeStringField("msg", value.getMessage());
        gen.writeStringField("data", value.getErrorCode());
        gen.writeEndObject();
    }
}
