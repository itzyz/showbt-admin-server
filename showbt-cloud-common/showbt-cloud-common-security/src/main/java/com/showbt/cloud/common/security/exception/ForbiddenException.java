package com.showbt.cloud.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.showbt.cloud.common.security.component.ShowbtAuth2ExceptionSerializer;
import org.springframework.http.HttpStatus;

@JsonSerialize(using = ShowbtAuth2ExceptionSerializer.class)
public class ForbiddenException extends ShowbtAuth2Exception {

    public ForbiddenException(String msg, Throwable t) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "access_denied";
    }

    @Override
    public int getHttpErrorCode() {
        return HttpStatus.FORBIDDEN.value();
    }

}

