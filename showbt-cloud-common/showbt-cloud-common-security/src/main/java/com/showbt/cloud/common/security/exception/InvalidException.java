package com.showbt.cloud.common.security.exception;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.showbt.cloud.common.security.component.ShowbtAuth2ExceptionSerializer;

@JsonSerialize(using = ShowbtAuth2ExceptionSerializer.class)
public class InvalidException extends ShowbtAuth2Exception {

    public InvalidException(String msg, Throwable t) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "invalid_exception";
    }

    @Override
    public int getHttpErrorCode() {
        return 426;
    }

}
