package com.showbt.cloud.common.core.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class HttpClientUtils {
    private static final int TIME_OUT = 3000;
    private static final String DEFAULT_ENCODE = StandardCharsets.UTF_8.name();

    public static String sendGet(String url) throws IOException {
        return sendGet(url, TIME_OUT);
    }

    /**
     * 发送 GET 请求
     *
     * @param url     Path
     * @param timeout 超时时间
     * @return
     * @throws IOException
     */
    public static String sendGet(String url, int timeout) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)
                .setSocketTimeout(timeout).build();
        httpGet.setConfig(requestConfig);
        CloseableHttpResponse response = client.execute(httpGet);
        HttpEntity entity = getEntity(response);
        if (entity != null)
            return EntityUtils.toString(entity);
        client.close();
        return null;
    }


    public static String sendPostFormData(String url, Map<String, Object> data) throws IOException {
        return sendPostFormData(url, data, TIME_OUT);
    }

    /**
     * 以 form-data 的格式发送 POST 请求
     *
     * @param url     请求地址
     * @param data    请求数据
     * @param timeout 超时时间
     * @return 返回响应结果
     */
    public static String sendPostFormData(String url, Map<String, Object> data, int timeout) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)
                .setSocketTimeout(timeout).build();
        httpPost.setConfig(requestConfig);
        List<NameValuePair> body = new ArrayList<>();
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            body.add(new BasicNameValuePair(entry.getKey(), (String) entry.getValue()));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(body, StandardCharsets.UTF_8));
        if (client == null) {
            client = HttpClients.createDefault();
        }
        CloseableHttpResponse response = client.execute(httpPost);
        HttpEntity entity = getEntity(response);
        if (entity != null)
            return EntityUtils.toString(entity);
        client.close();
        return null;
    }

    public static String sendPostRaw(String url, String json, Map<String, String> headers) throws IOException {
        return sendPostRaw(url, json, headers, DEFAULT_ENCODE, TIME_OUT);
    }

    /**
     * 以 RAW 格式发送 POST 请求
     *
     * @param url     请求地址
     * @param json    请求数据
     * @param headers 请求头
     * @param encode  请求编码格式
     * @param timeout 超时时间
     * @return 返回响应结果
     */
    public static String sendPostRaw(String url, String json, Map<String, String> headers, String encode, int timeout) throws IOException {
        if (StringUtils.isEmpty(encode)) {
            encode = "utf-8";
        }
        CloseableHttpClient client = HttpClients.createDefault();
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)
                .setSocketTimeout(timeout).build();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(requestConfig);

        if (headers != null && headers.size() > 0) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPost.setHeader(entry.getKey(), entry.getValue());
            }
        }

        StringEntity stringEntity = new StringEntity(json, encode);
        httpPost.setEntity(stringEntity);
        CloseableHttpResponse response = client.execute(httpPost);
        HttpEntity entity = getEntity(response);
        if (entity != null)
            return EntityUtils.toString(entity);
        client.close();
        return null;
    }

    /**
     * 从response中获取entity
     *
     * @param response 响应
     * @return 返回entity
     */
    private static HttpEntity getEntity(CloseableHttpResponse response) {
        if (response != null) {
            int code = response.getStatusLine().getStatusCode();
            if (code == 200)
                return response.getEntity();
        }
        return null;
    }
}
