package com.showbt.cloud.common.core.exception;

import lombok.NoArgsConstructor;

/**
 * 403 授权拒绝
 */
@NoArgsConstructor
public class ShowbtDeniedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ShowbtDeniedException(String message) {
        super(message);
    }

    public ShowbtDeniedException(Throwable cause) {
        super(cause);
    }

    public ShowbtDeniedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ShowbtDeniedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
