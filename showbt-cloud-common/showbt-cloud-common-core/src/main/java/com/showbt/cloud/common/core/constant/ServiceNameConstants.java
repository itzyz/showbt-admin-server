package com.showbt.cloud.common.core.constant;

/**
 * 服务名称
 */
public interface ServiceNameConstants {
    /**
     * 认证服务的SERVICEID
     */
    String AUTH_SERVICE = "showbt-cloud-auth";

    /**
     * UMPS模块
     */
    String UMPS_SERVICE = "showbt-cloud-upms";

    /**
     * SPMS模块
     */
    String SPMS_SERVICE = "showbt-cloud-spms";

}
