package com.showbt.cloud.common.core.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Joiner;
import lombok.experimental.UtilityClass;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 转换工具类
 *
 * @author loris
 */
@UtilityClass
public class TransUtils {

    //默认utf8编码
    private final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

    /**
     * DataBuffer 转 String, 指定编码格式
     *
     * @param dataBuffer
     * @param charset
     * @return
     */
    public String transDataBufferToString(DataBuffer dataBuffer, Charset charset) {
        byte[] content = new byte[dataBuffer.readableByteCount()];
        dataBuffer.read(content);
        DataBufferUtils.release(dataBuffer);

        return new String(content, charset);
    }

    /**
     * DataBuffer 转 String, 使用默认编码
     *
     * @param dataBuffer
     * @return
     */
    public String transDataBufferToString(DataBuffer dataBuffer) {
        return transDataBufferToString(dataBuffer, DEFAULT_CHARSET);
    }

    /**
     * String 转 DataBuffer, 指定编码格式
     *
     * @param factory
     * @param str
     * @param charset
     * @return
     */
    public DataBuffer transStringToDataBuffer(DataBufferFactory factory, String str, Charset charset) {
        byte[] content = str.getBytes(charset);
        return factory.wrap(content);
    }

    /**
     * String 转 DataBuffer, 使用默认编码
     *
     * @param factory
     * @param str
     * @return
     */
    public DataBuffer transStringToDataBuffer(DataBufferFactory factory, String str) {
        return transStringToDataBuffer(factory, str, DEFAULT_CHARSET);
    }

    /**
     * String 转 Map
     *
     * @param str
     * @return
     */
    public Map<String, Object> transStringToMap(String str) {
        Map<String, Object> map = new HashMap<>();
        String[] bodySplit = str.split("&");
        for (String param : bodySplit) {
            String key = param.split("=")[0];
            String value = param.substring(param.indexOf("=") + 1);
            map.put(key, value);
        }
        return map;
    }

    /**
     * map 转 string
     * 格式: key=value&key=value&key=value
     * key按照字典序排序
     *
     * @param map
     * @return
     */
    public String transMapToString(Map<String, String> map) {
        List<String> list = new ArrayList<>(map.keySet());
        Collections.sort(list);
        StringBuilder sb = new StringBuilder();
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();
            sb.append(key);
            sb.append("=");
            sb.append(map.get(key));
            if (iterator.hasNext()) {
                sb.append("&");
            }
        }
        return sb.toString();
    }

    /**
     * JSON String 转 Map
     *
     * @param json
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, Object> transJsonToMap(String json) {
        JSONObject jsonObj = JSON.parseObject(json);
        return JSONObject.toJavaObject(jsonObj, Map.class);
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> transJsonToStrMap(String json) {
        JSONObject jsonObj = JSON.parseObject(json);
        return JSONObject.toJavaObject(jsonObj, Map.class);
    }

    /**
     * Trans list to String
     *
     * @param list
     * @return
     */
    public String transListToString(List<String> list) {
        return Joiner.on("").join(list);
    }

    /**
     * Trans json to MultiValueMap
     *
     * @param json
     * @return
     */
    public static MultiValueMap<String, Object> transJsonToMultiValueMap(JSONObject json) {
        MultiValueMap<String, Object> multiValueMap = new LinkedMultiValueMap<>();
        for (String key : json.keySet()) {
            multiValueMap.add(key, json.get(key));
        }
        return multiValueMap;
    }
}
