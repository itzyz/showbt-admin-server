package com.showbt.cloud.common.core.util;

import lombok.experimental.UtilityClass;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@UtilityClass
public class JavaBeanUtils {
    /**
     * 获取对象中值为null的字段名
     *
     * @param source    源对象
     * @return          空值字段名数组
     */
    public String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue instanceof Collection) {
                if (((Collection) srcValue).isEmpty()) emptyNames.add(pd.getName());
            }else if (srcValue instanceof Map){
                if (((Map) srcValue).isEmpty()) emptyNames.add(pd.getName());
            }else {
                if (srcValue == null) emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }

    /**
     * 获取不为空的属性
     *
     * @param source
     * @return
     */
    public String[] getNoNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();
        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue != null) {
                if (srcValue instanceof Collection) {
                    Collection x = (Collection) srcValue;
                    if (!x.isEmpty()) {
                        emptyNames.add(pd.getName());
                    }
                } else if (srcValue instanceof Map) {
                    Map m = (Map) srcValue;
                    if (!m.isEmpty()) {
                        emptyNames.add(pd.getName());
                    }
                } else {
                    if (!pd.getName().equals("class") && !pd.getName().equals("status"))
                        emptyNames.add(pd.getName());
                }
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
