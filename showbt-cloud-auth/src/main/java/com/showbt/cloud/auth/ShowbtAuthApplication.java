package com.showbt.cloud.auth;

import com.showbt.cloud.common.security.annotation.EnableShowbtFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 认证授权中心
 */
@EnableCaching
@EnableJpaRepositories
@EnableTransactionManagement
@SpringCloudApplication
@EnableShowbtFeignClients
public class ShowbtAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(ShowbtAuthApplication.class, args);
    }
}
