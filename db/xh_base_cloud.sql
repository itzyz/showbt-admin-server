-- MySQL dump 10.13  Distrib 8.0.20, for Linux (x86_64)
--
-- Host: localhost    Database: xh_base_cloud
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cpt_app`
--

DROP TABLE IF EXISTS `cpt_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_app` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `apikey` varchar(255) DEFAULT NULL,
  `app_name` varchar(255) DEFAULT NULL,
  `pid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK175oe7eblb5op6fsxgoxnpt93` (`pid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_app`
--

LOCK TABLES `cpt_app` WRITE;
/*!40000 ALTER TABLE `cpt_app` DISABLE KEYS */;
INSERT INTO `cpt_app` VALUES (1,NULL,'2020-03-17 09:21:34','',1,NULL,'2020-03-17 15:28:11','1652f3f8082945a8a67f7d17c076e229','test',NULL);
/*!40000 ALTER TABLE `cpt_app` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_app_server`
--

DROP TABLE IF EXISTS `cpt_app_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_app_server` (
  `app_id` bigint(20) NOT NULL,
  `server_id` varchar(32) NOT NULL,
  KEY `FKl6aqtjbfnl19dugy59ids8cgm` (`server_id`),
  KEY `FKpdpvtgu0cwob9yx6wrc5ocyky` (`app_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_app_server`
--

LOCK TABLES `cpt_app_server` WRITE;
/*!40000 ALTER TABLE `cpt_app_server` DISABLE KEYS */;
INSERT INTO `cpt_app_server` VALUES (1,'8afd8aed6f6930aa0170e272c828006b'),(1,'8afd8aed6f6930aa0170e2735d97006c'),(1,'8afd8aed6f6930aa0170e273d55f006d'),(1,'8afd8aed6f6930aa0170e2741f51006e'),(1,'8afd8aed6f6930aa0170e2a37e84007d'),(1,'8afd8aed6f6930aa0170e27495ed006f'),(1,'8afd8aed6f6930aa0170e275049e0070'),(1,'8afd8aed6f6930aa0170e27581ce0071'),(1,'8afd8aed6f6930aa016fb113511e0056'),(1,'8afd8aed6f6930aa0170e2ab5d260098'),(1,'8afd8aed6f6930aa0170e2a3c9bb007e'),(1,'8afd8aed6f6930aa0170e2aba06f0099'),(1,'8afd8aed6f6930aa0170e29d9b580072'),(1,'8afd8aed6f6930aa0170e2a41e15007f'),(1,'8afd8aed6f6930aa0170e2a6da360086'),(1,'8afd8aed6f6930aa0170e2a718230087'),(1,'8afd8aed6f6930aa0170e2a756850088'),(1,'8afd8aed6f6930aa0170e2a7a3a50089'),(1,'8afd8aed6f6930aa0170e2a7d6ec008a'),(1,'8afd8aed6f6930aa0170e29dd8c10073'),(1,'8afd8aed6f6930aa0170e2a80c45008b'),(1,'8afd8aed6f6930aa0170e29e1cab0074'),(1,'8afd8aed6f6930aa0170e2a84bfc008c'),(1,'8afd8aed6f6930aa0170e29e65a30075'),(1,'8afd8aed6f6930aa0170e2aa762b0094'),(1,'8afd8aed6f6930aa016fb11391290057'),(1,'8afd8aed6f6930aa0170e2a1615f0076'),(1,'8afd8aed6f6930aa0170e2a2c3e7007b'),(1,'8afd8aed6f6930aa0170e27068870068'),(1,'8afd8aed6f6930aa0170e2aaada00095'),(1,'8afd8aed6f6930aa0170e2a4f9a10082'),(1,'8afd8aed6f6930aa0170e2722d8d0069'),(1,'8afd8aed6f6930aa0170e2a569ce0083'),(1,'8afd8aed6f6930aa0170e2a4877e0080'),(1,'8afd8aed6f6930aa0170e2727dea006a'),(1,'8afd8aed6f6930aa0170e2a5c6320084'),(1,'8afd8aed6f6930aa0170e2aaeab10096'),(1,'8afd8aed6f6930aa0170e2a4b71d0081'),(1,'8afd8aed6f6930aa0170e2a89085008d'),(1,'8afd8aed6f6930aa0170e2a8d445008e'),(1,'8afd8aed6f6930aa0170e2a9224d008f'),(1,'8afd8aed6f6930aa0170e2a33077007c'),(1,'8afd8aed6f6930aa016fb10c9feb004d'),(1,'8afd8aed6f6930aa0170e2a1a5230077'),(1,'8afd8aed6f6930aa016fb10ce23b004e'),(1,'8afd8aed6f6930aa0170e2a1e4c40078'),(1,'8afd8aed6f6930aa0170e2a22a9e0079'),(1,'8afd8aed6f6930aa0170e2a2821e007a'),(1,'8afd8aed6f6930aa016fb10d2062004f'),(1,'8afd8aed6f6930aa016fb11e58f9005c'),(1,'8afd8aed6f6930aa016fb11edc20005d'),(1,'8afd8aed6f6930aa016fb10d9c790050'),(1,'8afd8aed6f6930aa016fb11f2f40005e'),(1,'8afd8aed6f6930aa016fb120e56b0060'),(1,'8afd8aed6f6930aa016fb10e0a850051'),(1,'8afd8aed6f6930aa016fb10c50d0004c'),(1,'8afd8aed6f6930aa016fb113ea980058'),(1,'8afd8aed6f6930aa016fb10e61e30052'),(1,'8afd8aed6f6930aa016fb1198e870059'),(1,'8afd8aed6f6930aa016fb11d3139005a'),(1,'8afd8aed6f6930aa016fb10f05440053'),(1,'8afd8aed6f6930aa016fb11e1367005b'),(1,'8afd8aed6f6930aa0170e2a95d950090'),(1,'8afd8aed6f6930aa016fb10f73a50054'),(1,'8afd8aed6f6930aa0170e2a98e0f0091'),(1,'8afd8aed6f6930aa0170e2a9cfa90092'),(1,'8afd8aed6f6930aa0170e2ab34640097'),(1,'8afd8aed6f6930aa016fb10ff6610055'),(1,'8afd8aed6f6930aa0170e2aa26290093'),(1,'8afd8aed6f6930aa0170e2a614920085');
/*!40000 ALTER TABLE `cpt_app_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_module`
--

DROP TABLE IF EXISTS `cpt_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_module` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `health` varchar(255) DEFAULT NULL,
  `label` varchar(120) DEFAULT NULL,
  `module_name` varchar(120) DEFAULT NULL,
  `real_path` varchar(255) DEFAULT NULL,
  `p_id` varchar(32) DEFAULT NULL,
  `ips` text,
  `protocol` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKcwcynswufqa7oe3t0a93hyidd` (`label`,`p_id`),
  KEY `FK4hym9sax4w7wnwiatja02dy16` (`p_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_module`
--

LOCK TABLES `cpt_module` WRITE;
/*!40000 ALTER TABLE `cpt_module` DISABLE KEYS */;
INSERT INTO `cpt_module` VALUES ('8afd8aed6f6930aa016fa7cf2c860018',NULL,'2020-01-15 14:06:10','',1,NULL,'2020-01-17 09:24:37','','/tasks','任务管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa7d0dfe70019',NULL,'2020-01-15 14:08:02','',1,NULL,'2020-01-17 09:24:33','','/app','应用管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa819f590001a',NULL,'2020-01-15 15:27:51','',1,NULL,'2020-01-17 09:24:30','','/module','模块管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa86eaa9f0023',NULL,'2020-01-15 17:00:23','',1,NULL,'2020-01-17 09:24:26','','/project','项目管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa86eeffe0024',NULL,'2020-01-15 17:00:40','',1,NULL,'2020-01-17 09:24:23','','/server','服务管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa86f22060025',NULL,'2020-01-15 17:00:53','',1,NULL,'2020-01-17 09:24:10','','/strategy','策略管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('8afd8aed6f6930aa016fa86f93500026',NULL,'2020-01-15 17:01:22','',1,NULL,'2020-01-17 09:24:17','','/rules','规则管理','lb://showbt-cloud-spms','8afd8aed6f6930aa016fa7cd516c0017',NULL,NULL),('1001',NULL,NULL,NULL,1,NULL,NULL,NULL,'/oauth','授权模块','lb://showbt-cloud-auth','1',NULL,NULL),('8afd8aed6e6406ed016e8297064d000e',NULL,'2019-11-19 15:36:06','',1,NULL,'2019-11-20 17:15:41',NULL,'/dept','部门管理','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL),('8afd8aed6e6406ed016e82975545000f',NULL,'2019-11-19 15:36:26','',1,NULL,'2019-11-20 17:15:37',NULL,'/client','前端控制器','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL),('8afd8aed6e6406ed016e8297cf7e0010',NULL,'2019-11-19 15:36:57','',1,NULL,'2019-11-20 17:15:32',NULL,'/token','token管理','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL),('8afd8aed6e6406ed016e82982c690011',NULL,'2019-11-19 15:37:21','',1,NULL,'2019-11-20 17:15:27',NULL,'/user','用户管理','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL),('8afd8aed6e6406ed016e829873570012',NULL,'2019-11-19 15:37:39','',1,NULL,'2019-11-20 17:15:23',NULL,'/role','角色管理','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL),('8afd8aed6e6406ed016e8bfb71ca001c',NULL,'2019-11-21 11:22:22','',1,NULL,'2019-11-21 11:28:19',NULL,'/menu','菜单','lb://showbt-cloud-upms','8afd8aed6e6406ed016e8295565f000d',NULL,NULL);
/*!40000 ALTER TABLE `cpt_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_project`
--

DROP TABLE IF EXISTS `cpt_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_project` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK27tmoq9eyrprsyd2mvwkdpfyh` (`label`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_project`
--

LOCK TABLES `cpt_project` WRITE;
/*!40000 ALTER TABLE `cpt_project` DISABLE KEYS */;
INSERT INTO `cpt_project` VALUES ('8afd8aed6f6930aa016fa7cd516c0017','','2020-01-15 14:04:08','',1,'','2020-03-16 17:26:18','/spms','服务权限管理'),('1',NULL,NULL,'统一用户登录入口',1,NULL,'2020-11-08 21:07:39','/auth','用户认证系统'),('2',NULL,NULL,NULL,0,NULL,NULL,'/test01','test001'),('3',NULL,NULL,NULL,0,NULL,NULL,'/test02','test002'),('8afd8aed6e6406ed016e8295565f000d',NULL,'2019-11-19 15:34:15','',1,NULL,NULL,'/admin','用户管理系统');
/*!40000 ALTER TABLE `cpt_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_rule_dict`
--

DROP TABLE IF EXISTS `cpt_rule_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_rule_dict` (
  `id` varchar(32) NOT NULL,
  `args` text,
  `rule_name` varchar(100) DEFAULT NULL,
  `rule_type` varchar(20) DEFAULT NULL,
  `remark` text,
  PRIMARY KEY (`id`),
  KEY `IDX2svhqct2ubakausqixcyvv3yt` (`rule_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_rule_dict`
--

LOCK TABLES `cpt_rule_dict` WRITE;
/*!40000 ALTER TABLE `cpt_rule_dict` DISABLE KEYS */;
INSERT INTO `cpt_rule_dict` VALUES ('8afd8aed6e3a687d016e3a6e13510003',NULL,'SenseServiceCheckSigFilter','filter',NULL),('8afd8aed6e6406ed016e8b9ea6c00017',NULL,'PasswordDecoderFilter','filter',NULL),('8afd8aed6e6406ed016e8b9ef4050018',NULL,'ValidateCodeGatewayFilter','filter',NULL),('8afd8aed6e6406ed016ea5552bbe002d',NULL,'Auth','filter',NULL),('8afd8aed6ea6ed16016eedc4a1b50031','[\"keyResolver\",\"redis-rate-limiter.replenishRate\",\"redis-rate-limiter.burstCapacity\"]','RequestRateLimiter','filter','# 限流配置\n            - name: RequestRateLimiter\n              args:\n                key-resolver: \'#{@remoteAddrKeyResolver}\'\n                redis-rate-limiter.replenishRate: 10\n                redis-rate-limiter.burstCapacity: 20'),('8afd8aed6ea6ed16016eedc52a6b0032','[\"name\",\"fallbackUri\"]','Hystrix','filter','# 降级配置\n            - name: Hystrix\n              args:\n                name: default	\n                fallbackUri: \'forward:/fallback\''),('8afd8aed6ef3b560016ef43886dd0000','[\"name\",\"value\"]','AddResponseHeader','filter',NULL);
/*!40000 ALTER TABLE `cpt_rule_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_server`
--

DROP TABLE IF EXISTS `cpt_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_server` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `label` varchar(32) DEFAULT NULL,
  `real_path` text NOT NULL,
  `server_name` varchar(100) DEFAULT NULL,
  `m_id` varchar(32) DEFAULT NULL,
  `fixed_params` varchar(255) DEFAULT NULL,
  `mapping_fields` varchar(255) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `request_param_keys` varchar(255) DEFAULT NULL,
  `g_id` bigint(20) DEFAULT NULL,
  `custom_param` text,
  `formatter_class` varchar(255) DEFAULT NULL,
  `request_headers` varchar(255) DEFAULT NULL,
  `request_key_mapping` varchar(255) DEFAULT NULL,
  `server_type` varchar(255) DEFAULT NULL,
  `v1_g_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKm5n1xp0yuj9qgan1sxheg1kjp` (`label`,`m_id`),
  KEY `IDX53elmhmck2wl7p661xqbi7x3g` (`label`),
  KEY `FKl9028gik39i8262ev9eekgq8x` (`g_id`),
  KEY `FKl0ml13whrb3p9p9yq5velmhh0` (`m_id`),
  KEY `FK3ij18ssap4c5wye2jxf5rm792` (`v1_g_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_server`
--

LOCK TABLES `cpt_server` WRITE;
/*!40000 ALTER TABLE `cpt_server` DISABLE KEYS */;
INSERT INTO `cpt_server` VALUES ('8afd8aed6f6930aa0170e2a95d950090',NULL,'2020-03-16 17:25:15','',1,NULL,NULL,'/list','/task/list','全量查询','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a98e0f0091',NULL,'2020-03-16 17:25:28','',1,NULL,NULL,'/page','/task','全量查询－分页','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a9cfa90092',NULL,'2020-03-16 17:25:44','',1,NULL,NULL,'/search','/task/list/search','条件查询','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2aa26290093',NULL,'2020-03-16 17:26:07','',1,NULL,NULL,'/search-page','/task/search','条件查询－分页','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2aa762b0094',NULL,'2020-03-16 17:26:27','',1,NULL,NULL,'/find-by-id','/task','根据ID查询','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2aaada00095',NULL,'2020-03-16 17:26:41','',1,NULL,NULL,'/exists','/task/exists','唯一性验证','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2aaeab10096',NULL,'2020-03-16 17:26:57','',1,NULL,NULL,'/remove','/task','根据ID删除','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2ab34640097',NULL,'2020-03-16 17:27:16','',1,NULL,NULL,'/batch-remove','/task/batchRemove','批量删除','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2ab5d260098',NULL,'2020-03-16 17:27:26','',1,NULL,NULL,'/save','/task','单条入库','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2aba06f0099',NULL,'2020-03-16 17:27:43','',1,NULL,NULL,'/partial-update','/task/partialUpdate','部分更新','8afd8aed6f6930aa016fa7cf2c860018',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a6da360086',NULL,'2020-03-16 17:22:31','',1,NULL,NULL,'/list','/app/list','全量查询','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a718230087',NULL,'2020-03-16 17:22:46','',1,NULL,NULL,'/page','/app','全量查询－分页','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a756850088',NULL,'2020-03-16 17:23:02','',1,NULL,NULL,'/search','/app/list/search','条件查询','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a7a3a50089',NULL,'2020-03-16 17:23:22','',1,NULL,NULL,'/search-page','/app/search','条件查询－分页','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a7d6ec008a',NULL,'2020-03-16 17:23:35','',1,NULL,NULL,'/exists','/app/exists','唯一性验证','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a80c45008b',NULL,'2020-03-16 17:23:49','',1,NULL,NULL,'/find-by-id','/app','根据ID查询','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a84bfc008c',NULL,'2020-03-16 17:24:05','',1,NULL,NULL,'/remove','/app','根据ID删除','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a89085008d',NULL,'2020-03-16 17:24:23','',1,NULL,NULL,'/partial-update','/app/partialUpdate','部分更新','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a8d445008e',NULL,'2020-03-16 17:24:40','',1,NULL,NULL,'/save','/app','单条入库','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a9224d008f',NULL,'2020-03-16 17:25:00','',1,NULL,NULL,'/batch-remove','/app/batchRemove','批量删除','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a33077007c',NULL,'2020-03-16 17:18:31','',1,NULL,NULL,'/list','/module/list','全量查询','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a37e84007d',NULL,'2020-03-16 17:18:51','',1,NULL,NULL,'/page','/module','全量查询－分页','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a3c9bb007e',NULL,'2020-03-16 17:19:10','',1,NULL,NULL,'/search','/module/list/search','条件查询','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a41e15007f',NULL,'2020-03-16 17:19:31','',1,NULL,NULL,'/search-page','/module/search','条件查询－分页','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a4877e0080',NULL,'2020-03-16 17:19:58','',1,NULL,NULL,'/find-by-id','/module','根据ID查询','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a4b71d0081',NULL,'2020-03-16 17:20:11','',1,NULL,NULL,'/exists','/module/exists','唯一性验证','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a4f9a10082',NULL,'2020-03-16 17:20:28','',1,NULL,NULL,'/remove','/module','根据ID删除','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a569ce0083',NULL,'2020-03-16 17:20:56','',1,NULL,NULL,'/batch-remove','/module/batchRemove','批量删除','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a5c6320084',NULL,'2020-03-16 17:21:20','',1,NULL,NULL,'/partial-update','/module','部分更新','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a614920085',NULL,'2020-03-16 17:21:40','',1,NULL,NULL,'/save','/module','单条入库','8afd8aed6f6930aa016fa819f590001a',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e29d9b580072',NULL,'2020-03-16 17:12:25','',1,NULL,NULL,'/list','/project/list','全量查询','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e29dd8c10073',NULL,'2020-03-16 17:12:40','',1,NULL,NULL,'/page','/project','全量查询－分页','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e29e1cab0074',NULL,'2020-03-16 17:12:58','',1,NULL,'2020-03-16 17:13:27','/search','/project/list/search','条件查询','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e29e65a30075',NULL,'2020-03-16 17:13:16','',1,NULL,NULL,'/search-page','/project/search','条件查询－分页','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a1615f0076',NULL,'2020-03-16 17:16:32','',1,NULL,NULL,'/find-by-id','/project','根据ID查询','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a1a5230077',NULL,'2020-03-16 17:16:49','',1,NULL,NULL,'/exists','/project/exists','唯一性验证','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a1e4c40078',NULL,'2020-03-16 17:17:06','',1,NULL,NULL,'/remove','/project','根据ID删除','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a22a9e0079',NULL,'2020-03-16 17:17:23','',1,NULL,NULL,'/batch-remove','/project/batchRemove','批量删除','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a2821e007a',NULL,'2020-03-16 17:17:46','',1,NULL,NULL,'/partial-update','/project','部分更新','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2a2c3e7007b',NULL,'2020-03-16 17:18:03','',1,NULL,NULL,'/save','/project','单挑入库','8afd8aed6f6930aa016fa86eaa9f0023',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e27068870068',NULL,'2020-03-16 16:23:03','',1,NULL,NULL,'/list','/server/list','全量查询','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2722d8d0069',NULL,'2020-03-16 16:24:59','',1,NULL,NULL,'/page','/server','全量查询－分页','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2727dea006a',NULL,'2020-03-16 16:25:19','',1,NULL,NULL,'/search','/server/list/search','条件查询','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e272c828006b',NULL,'2020-03-16 16:25:38','',1,NULL,NULL,'/search-page','/server/search','条件查询－分页','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2735d97006c',NULL,'2020-03-16 16:26:16','',1,NULL,NULL,'/find-by-id','/server','根据ID查询','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e273d55f006d',NULL,'2020-03-16 16:26:47','',1,NULL,NULL,'/batch-remove','/server/batchRemove','批量删除','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e2741f51006e',NULL,'2020-03-16 16:27:06','',1,NULL,NULL,'/exists','/server/exists','唯一性验证','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e27495ed006f',NULL,'2020-03-16 16:27:36','',1,NULL,NULL,'/remove','/server','根据ID删除','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e275049e0070',NULL,'2020-03-16 16:28:05','',1,NULL,'2020-03-16 16:28:16','/partial-update','/server','部分更新','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa0170e27581ce0071',NULL,'2020-03-16 16:28:37','',1,NULL,'2020-11-21 21:17:29','/save','/server','单条入库','8afd8aed6f6930aa016fa86eeffe0024',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb113511e0056',NULL,'2020-01-17 09:17:11','',1,NULL,NULL,'/list','/strategy/list','全量查询','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11391290057',NULL,'2020-01-17 09:17:27','',1,NULL,NULL,'/page','/strategy','全量查询－分页','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb113ea980058',NULL,'2020-01-17 09:17:50','',1,NULL,NULL,'/search','/strategy/list/search','条件查询','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb1198e870059',NULL,'2020-01-17 09:24:00','',1,NULL,'2020-01-17 09:26:42','/search-page','/strategy/search','条件查询－分页','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11d3139005a',NULL,'2020-01-17 09:27:58','',1,NULL,NULL,'/find-by-id','/strategy','根据ID查询','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11e1367005b',NULL,'2020-01-17 09:28:56','',1,NULL,NULL,'/remove','/strategy','根据ID删除','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11e58f9005c',NULL,'2020-01-17 09:29:14','',1,NULL,NULL,'/exists','/strategy/exists','唯一性验证','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11edc20005d',NULL,'2020-01-17 09:29:47','',1,NULL,NULL,'/batch-remove','/strategy/batchRemove','批量删除','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb11f2f40005e',NULL,'2020-01-17 09:30:09','',1,NULL,NULL,'/partial-update','/strategy','部分更新','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb120e56b0060',NULL,'2020-01-17 09:32:01','',1,NULL,NULL,'/save','/strategy','单条入库','8afd8aed6f6930aa016fa86f22060025',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10c50d0004c',NULL,'2020-01-17 09:09:32','',1,NULL,NULL,'/list','/rule_dict/list','全量查询','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10c9feb004d',NULL,'2020-01-17 09:09:52','',1,NULL,NULL,'/page','/rule_dict','全量查询－分页','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10ce23b004e',NULL,'2020-01-17 09:10:09','',1,NULL,NULL,'/search','/rule_dict/list/search','条件查询','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10d2062004f',NULL,'2020-01-17 09:10:25','',1,NULL,'2020-01-17 09:25:49','/search-page','/rule_dict/search','条件查询－分页','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10d9c790050',NULL,'2020-01-17 09:10:57','',1,NULL,'2020-01-17 09:25:34','/find-by-id','/rule_dict','根据ID查询','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10e0a850051',NULL,'2020-01-17 09:11:25','',1,NULL,NULL,'/exists','/rule_dict/exists','唯一性验证','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10e61e30052',NULL,'2020-01-17 09:11:47','',1,NULL,'2020-01-17 09:12:02','/remove','/rule_dict','根据ID删除','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'DELETE',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10f05440053',NULL,'2020-01-17 09:12:29','',1,NULL,'2020-01-17 09:25:22','/batch-remove','/rule_dict/batchRemove','批量删除','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10f73a50054',NULL,'2020-01-17 09:12:58','',1,NULL,'2020-01-17 09:25:15','/partial-update','/rule_dict/partialUpdate','部分更新','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'PATCH',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6f6930aa016fb10ff6610055',NULL,'2020-01-17 09:13:31','',1,NULL,NULL,'/save','/rule_dict','单条入库','8afd8aed6f6930aa016fa86f93500026',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('1001001',NULL,NULL,NULL,1,NULL,NULL,'/token','/oauth/token','获取token','1001',NULL,NULL,'POST',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8afd8aed6ea6ed16016eab8616740006',NULL,'2019-11-27 14:22:02','',1,NULL,NULL,'/tree','/dept/tree','部门列表','8afd8aed6e6406ed016e8297064d000e',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6ea6ed16016eab97e7cf0008',NULL,'2019-11-27 14:41:30','',1,NULL,'2020-11-10 22:41:47','/index','/dept','部门首页','8afd8aed6e6406ed016e8297064d000e',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e8bdfcda8001b',NULL,'2019-11-21 10:52:11','',1,NULL,'2019-11-21 11:07:28','/info','/user/info','获取当前用户全部信息','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e91ec99f50024',NULL,'2019-11-22 15:03:53','',1,NULL,'2020-11-10 22:40:46','/index','/user/','通过ID查询或删除用户信息','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e91ed08850025',NULL,'2019-11-22 15:04:21','',1,NULL,'2020-03-18 16:05:00','/details','/user/details','根据用户名查询用户信息','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e91ef58df0026',NULL,'2019-11-22 15:06:53','',1,NULL,NULL,'/page','/user/page','分页查询用户','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e91efe9400027',NULL,'2019-11-22 15:07:29','',1,NULL,NULL,'/edit','/user/edit','修改个人信息','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e91f060290028',NULL,'2019-11-22 15:08:00','',1,NULL,NULL,'/ancestor','/user/ancestor','上级部门用户列表','8afd8aed6e6406ed016e82982c690011',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6ea6ed16016eab84e3dc0005',NULL,'2019-11-27 14:20:43','',1,NULL,'2019-11-27 14:21:13','/page','/role/page','角色列表','8afd8aed6e6406ed016e829873570012',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6ea6ed16016eab9a06d20009',NULL,'2019-11-27 14:43:49','',1,NULL,NULL,'/list','/role/list','角色列表1','8afd8aed6e6406ed016e829873570012',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6e6406ed016e8bfd0680001e',NULL,'2019-11-21 11:24:06','',1,NULL,'2020-11-10 22:41:18','/index','/menu/','菜单列表','8afd8aed6e6406ed016e8bfb71ca001c',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('8afd8aed6ea6ed16016eab9757100007',NULL,'2019-11-27 14:40:52','',1,NULL,NULL,'/tree','/menu/tree','菜单树','8afd8aed6e6406ed016e8bfb71ca001c',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,'G',NULL),('ff80818175e8fa110175eaf44f630000',NULL,'2020-11-21 21:18:01',NULL,1,NULL,NULL,'/getOne','/menu','根据ID获取菜单','8afd8aed6e6406ed016e8bfb71ca001c',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cpt_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_server_group`
--

DROP TABLE IF EXISTS `cpt_server_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_server_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `distinct_fields` varchar(255) DEFAULT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `mapping_keys` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_server_group`
--

LOCK TABLES `cpt_server_group` WRITE;
/*!40000 ALTER TABLE `cpt_server_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_server_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_server_rule`
--

DROP TABLE IF EXISTS `cpt_server_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_server_rule` (
  `args` text NOT NULL,
  `filter_or_rule_order` int(11) DEFAULT NULL,
  `rule_id` varchar(32) NOT NULL,
  `server_id` varchar(32) NOT NULL,
  PRIMARY KEY (`rule_id`,`server_id`),
  KEY `FKpcmimdukamw7qrsdu96y3fee8` (`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_server_rule`
--

LOCK TABLES `cpt_server_rule` WRITE;
/*!40000 ALTER TABLE `cpt_server_rule` DISABLE KEYS */;
INSERT INTO `cpt_server_rule` VALUES ('{}',NULL,'8afd8aed6e6406ed016e8b9ea6c00017','1001001');
/*!40000 ALTER TABLE `cpt_server_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_strategy`
--

DROP TABLE IF EXISTS `cpt_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_strategy` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `strategy_name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT '1',
  `app_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKr35gpv92dvdtbeici8hl43h3a` (`app_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_strategy`
--

LOCK TABLES `cpt_strategy` WRITE;
/*!40000 ALTER TABLE `cpt_strategy` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_strategy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_strategy_server`
--

DROP TABLE IF EXISTS `cpt_strategy_server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_strategy_server` (
  `config` varchar(255) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `server_id` varchar(32) NOT NULL,
  `strategy_id` bigint(20) NOT NULL,
  PRIMARY KEY (`server_id`,`strategy_id`),
  KEY `FK65cnixkadv3pmnmfv1v9ycov6` (`strategy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_strategy_server`
--

LOCK TABLES `cpt_strategy_server` WRITE;
/*!40000 ALTER TABLE `cpt_strategy_server` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_strategy_server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_strategy_type`
--

DROP TABLE IF EXISTS `cpt_strategy_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_strategy_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `type_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_strategy_type`
--

LOCK TABLES `cpt_strategy_type` WRITE;
/*!40000 ALTER TABLE `cpt_strategy_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_strategy_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_task`
--

DROP TABLE IF EXISTS `cpt_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_task` (
  `id` varchar(32) NOT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `effective_time` datetime DEFAULT NULL,
  `expiry_time` datetime DEFAULT NULL,
  `is_sign` int(11) DEFAULT '1',
  `task_name` varchar(255) DEFAULT NULL,
  `app_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK15qx6hwsr8b25vx7jwdm4po77` (`app_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_task`
--

LOCK TABLES `cpt_task` WRITE;
/*!40000 ALTER TABLE `cpt_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cpt_task_strategy`
--

DROP TABLE IF EXISTS `cpt_task_strategy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cpt_task_strategy` (
  `task_id` varchar(32) NOT NULL,
  `strategy_id` bigint(20) NOT NULL,
  KEY `FKepibuugah1ue39ofm8efvs315` (`strategy_id`),
  KEY `FKloct5dif4yb4usmml0vu7dr94` (`task_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cpt_task_strategy`
--

LOCK TABLES `cpt_task_strategy` WRITE;
/*!40000 ALTER TABLE `cpt_task_strategy` DISABLE KEYS */;
/*!40000 ALTER TABLE `cpt_task_strategy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (1);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept`
--

DROP TABLE IF EXISTS `sys_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dept` (
  `dept_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '部门名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '是否删除  -1：已删除  0：正常',
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='部门管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept`
--

LOCK TABLES `sys_dept` WRITE;
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` VALUES (10,'院校沙县',NULL,'2018-12-10 21:19:26',NULL,'0',8),(12,'新华网',1,'2019-05-16 14:47:38',NULL,'0',0),(13,'数据应用中心',1,'2019-05-16 14:48:19',NULL,'0',12),(14,'创新中心',2,'2019-05-16 14:48:35',NULL,'0',12);
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dept_relation`
--

DROP TABLE IF EXISTS `sys_dept_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dept_relation` (
  `ancestor` int(11) NOT NULL COMMENT '祖先节点',
  `descendant` int(11) NOT NULL COMMENT '后代节点',
  PRIMARY KEY (`ancestor`,`descendant`),
  KEY `idx1` (`ancestor`),
  KEY `idx2` (`descendant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='部门关系表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dept_relation`
--

LOCK TABLES `sys_dept_relation` WRITE;
/*!40000 ALTER TABLE `sys_dept_relation` DISABLE KEYS */;
INSERT INTO `sys_dept_relation` VALUES (10,10),(12,12),(12,13),(12,14),(13,13),(14,14);
/*!40000 ALTER TABLE `sys_dept_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_dict`
--

DROP TABLE IF EXISTS `sys_dict`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_dict` (
  `id` int(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `value` varchar(100) NOT NULL COMMENT '数据值',
  `label` varchar(100) NOT NULL COMMENT '标签名',
  `type` varchar(100) NOT NULL COMMENT '类型',
  `description` varchar(100) NOT NULL COMMENT '描述',
  `sort` int(10) NOT NULL COMMENT '排序（升序）',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `del_flag` char(1) NOT NULL DEFAULT '0' COMMENT '删除标记',
  PRIMARY KEY (`id`),
  KEY `sys_dict_value` (`value`),
  KEY `sys_dict_label` (`label`),
  KEY `sys_dict_del_flag` (`del_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_dict`
--

LOCK TABLES `sys_dict` WRITE;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` VALUES (1,'9','异常','log_type','日志异常',1,'2018-07-09 06:16:14','2018-11-24 07:25:11','日志异常','0'),(2,'0','正常','log_type','正常',0,'2018-07-09 06:15:40','2018-11-24 07:25:14','正常','0');
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_log`
--

DROP TABLE IF EXISTS `sys_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_log` (
  `id` bigint(64) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` char(1) DEFAULT '1' COMMENT '日志类型',
  `title` varchar(255) DEFAULT '' COMMENT '日志标题',
  `service_id` varchar(32) DEFAULT NULL COMMENT '服务ID',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remote_addr` varchar(255) DEFAULT NULL COMMENT '操作IP地址',
  `user_agent` varchar(1000) DEFAULT NULL COMMENT '用户代理',
  `request_uri` varchar(255) DEFAULT NULL COMMENT '请求URI',
  `method` varchar(10) DEFAULT NULL COMMENT '操作方式',
  `params` text COMMENT '操作提交的数据',
  `time` mediumtext COMMENT '执行时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标记',
  `exception` text COMMENT '异常信息',
  PRIMARY KEY (`id`),
  KEY `sys_log_create_by` (`create_by`),
  KEY `sys_log_request_uri` (`request_uri`),
  KEY `sys_log_type` (`type`),
  KEY `sys_log_create_date` (`create_time`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='日志表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_log`
--

LOCK TABLES `sys_log` WRITE;
/*!40000 ALTER TABLE `sys_log` DISABLE KEYS */;
INSERT INTO `sys_log` VALUES (51,'0','添加角色','test','admin','2019-01-24 20:56:43',NULL,'0:0:0:0:0:0:0:1','PostmanRuntime/7.6.0','/role','POST','Authorization=%5B%5D','65','0',NULL),(52,'0','添加部门','showbt','admin','2019-05-16 14:47:39',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/dept','POST','','50','0',NULL),(53,'0','删除部门','showbt','admin','2019-05-16 14:47:58',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/dept/1','DELETE','','285','0',NULL),(54,'0','添加部门','showbt','admin','2019-05-16 14:48:19',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/dept','POST','','102','0',NULL),(55,'0','添加部门','showbt','admin','2019-05-16 14:48:35',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/dept','POST','','147','0',NULL),(56,'0','删除部门','showbt','admin','2019-05-16 14:48:41',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/dept/2','DELETE','','49','0',NULL),(57,'0','更新菜单','showbt','admin','2019-05-16 16:31:59',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/menu','PUT','','149','0',NULL),(58,'0','删除终端','showbt','admin','2019-05-16 16:36:42',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/client/pig','DELETE','','147','0',NULL),(59,'0','更新菜单','showbt','admin','2019-05-17 16:56:58',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/menu','PUT','','152','0',NULL),(60,'0','编辑终端','showbt','admin','2019-05-17 17:07:14',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/client','PUT','','53','0',NULL),(61,'0','编辑终端','showbt','admin','2019-05-17 17:07:42',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/client','PUT','','4','0',NULL),(62,'0','添加用户','showbt','admin','2019-05-20 14:44:41',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/user','POST','','196','0',NULL),(63,'0','更新用户信息','showbt','admin','2019-05-20 14:44:48',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/user','PUT','','217','0',NULL),(64,'0','添加角色','showbt','admin','2019-05-20 14:45:01',NULL,'127.0.0.1','Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36','/role','POST','','46','0',NULL);
/*!40000 ALTER TABLE `sys_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  `name` varchar(32) NOT NULL COMMENT '菜单名称',
  `permission` varchar(32) DEFAULT NULL COMMENT '菜单权限标识',
  `path` varchar(128) DEFAULT NULL COMMENT '前端URL',
  `parent_id` int(11) DEFAULT NULL COMMENT '父菜单ID',
  `icon` varchar(32) DEFAULT NULL COMMENT '图标',
  `component` varchar(64) DEFAULT NULL COMMENT 'VUE页面',
  `sort` int(11) DEFAULT '1' COMMENT '排序值',
  `keep_alive` char(1) DEFAULT '0' COMMENT '0-开启，1- 关闭',
  `type` char(1) DEFAULT NULL COMMENT '菜单类型 （0菜单 1按钮）',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `del_flag` char(1) DEFAULT '0' COMMENT '逻辑删除标记(0--正常 1--删除)',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` VALUES (1000,'权限管理',NULL,'/admin/upms',-1,'icon-quanxianguanli','Layout',0,'0','0','2018-09-28 08:29:53','2020-11-18 12:53:41','0'),(1100,'用户管理',NULL,'/user',1000,'icon-yonghuguanli','/admin/upms/user',1,'0','0','2017-11-02 22:24:37','2020-11-20 13:34:04','0'),(1101,'用户新增','sys_user_add',NULL,1100,NULL,NULL,NULL,'0','1','2017-11-08 09:52:09','2018-09-28 09:06:34','0'),(1102,'用户修改','sys_user_edit',NULL,1100,NULL,NULL,NULL,'0','1','2017-11-08 09:52:48','2018-09-28 09:06:37','0'),(1103,'用户删除','sys_user_del',NULL,1100,NULL,NULL,NULL,'0','1','2017-11-08 09:54:01','2018-09-28 09:06:42','0'),(1200,'菜单管理',NULL,'/menu',1000,'icon-caidanguanli','/admin/upms/menu',2,'0','0','2017-11-08 09:57:27','2020-11-20 13:34:04','0'),(1201,'菜单新增','sys_menu_add',NULL,1200,NULL,NULL,NULL,'0','1','2017-11-08 10:15:53','2018-09-28 09:07:16','0'),(1202,'菜单修改','sys_menu_edit',NULL,1200,NULL,NULL,NULL,'0','1','2017-11-08 10:16:23','2018-09-28 09:07:18','0'),(1203,'菜单删除','sys_menu_del',NULL,1200,NULL,NULL,NULL,'0','1','2017-11-08 10:16:43','2018-09-28 09:07:22','0'),(1300,'角色管理',NULL,'/role',1000,'icon-jiaoseguanli','/admin/upms/role',3,'0','0','2017-11-08 10:13:37','2020-11-20 13:34:04','0'),(1301,'角色新增','sys_role_add',NULL,1300,NULL,NULL,NULL,'0','1','2017-11-08 10:14:18','2018-09-28 09:07:46','0'),(1302,'角色修改','sys_role_edit',NULL,1300,NULL,NULL,NULL,'0','1','2017-11-08 10:14:41','2018-09-28 09:07:49','0'),(1303,'角色删除','sys_role_del',NULL,1300,NULL,NULL,NULL,'0','1','2017-11-08 10:14:59','2018-09-28 09:07:53','0'),(1304,'分配权限','sys_role_perm',NULL,1300,NULL,NULL,NULL,'0','1','2018-04-20 07:22:55','2018-09-28 09:13:23','0'),(1400,'部门管理',NULL,'/dept',1000,'icon-web-icon-','/admin/upms/depart',4,'0','0','2018-01-20 13:17:19','2020-11-20 13:34:04','0'),(1401,'部门新增','sys_dept_add',NULL,1400,NULL,NULL,NULL,'0','1','2018-01-20 14:56:16','2018-09-28 09:08:13','0'),(1402,'部门修改','sys_dept_edit',NULL,1400,NULL,NULL,NULL,'0','1','2018-01-20 14:56:59','2018-09-28 09:08:16','0'),(1403,'部门删除','sys_dept_del',NULL,1400,NULL,NULL,NULL,'0','1','2018-01-20 14:57:28','2018-09-28 09:08:18','0'),(2000,'系统管理',NULL,'/admin/system',-1,'icon-xitongguanli','Layout',1,'0','0','2017-11-07 20:56:00','2020-11-18 12:53:41','0'),(2100,'日志管理',NULL,'/log',2000,'icon-rizhiguanli','/admin/system/log',5,'0','0','2017-11-20 14:06:22','2020-11-20 13:34:04','0'),(2101,'日志删除','sys_log_del',NULL,2100,NULL,NULL,NULL,'0','1','2017-11-20 20:37:37','2018-09-28 09:08:44','0'),(2200,'字典管理',NULL,'/dict',2000,'icon-navicon-zdgl','/admin/system/dict',6,'0','0','2017-11-29 11:30:52','2020-11-20 13:34:04','0'),(2201,'字典删除','sys_dict_del',NULL,2200,NULL,NULL,NULL,'0','1','2017-11-29 11:30:11','2018-09-28 09:09:10','0'),(2202,'字典新增','sys_dict_add',NULL,2200,NULL,NULL,NULL,'0','1','2018-05-11 22:34:55','2018-09-28 09:09:12','0'),(2203,'字典修改','sys_dict_edit',NULL,2200,NULL,NULL,NULL,'0','1','2018-05-11 22:36:03','2018-09-28 09:09:16','0'),(2300,'代码生成','','/gen',2000,'icon-weibiaoti46','/admin/system/gen',8,'0','0','2018-01-20 13:17:19','2020-11-20 13:34:04','0'),(2400,'终端管理','','/client',2000,'icon-shouji','/admin/system/client',9,'0','0','2018-01-20 13:17:19','2020-11-20 13:34:04','0'),(2401,'客户端新增','sys_client_add',NULL,2400,'1',NULL,NULL,'0','1','2018-05-15 21:35:18','2018-09-28 09:10:25','0'),(2402,'客户端修改','sys_client_edit',NULL,2400,NULL,NULL,NULL,'0','1','2018-05-15 21:37:06','2018-09-28 09:10:27','0'),(2403,'客户端删除','sys_client_del',NULL,2400,NULL,NULL,NULL,'0','1','2018-05-15 21:39:16','2018-09-28 09:10:30','0'),(2500,'服务监控',NULL,'http://172.18.24.228:5001',2000,'icon-server',NULL,10,'0','0','2018-06-26 10:50:32','2019-02-01 20:41:30','0'),(2600,'令牌管理',NULL,'/token',2000,'icon-denglvlingpai','/admin/system/token',11,'0','0','2018-09-04 05:58:41','2020-11-20 13:34:04','0'),(2601,'令牌删除','sys_token_del',NULL,2600,NULL,NULL,1,'0','1','2018-09-04 05:59:50','2018-09-28 09:11:24','0'),(5000,'一级菜单',NULL,'/crud',-1,'icon-caidanguanli','',4,'0','0','2018-08-28 01:50:22','2018-09-28 08:58:20','0'),(5001,'一级菜单',NULL,'/index',5000,'icon-caidanguanli','/admin/dashboard/dashboard',1,'0','0','2018-08-28 01:50:48','2020-11-20 13:34:04','1'),(5002,'二级菜单',NULL,'/crud',5001,'icon-caidanguanli','/admin/dashboard/dashboard',1,'0','0','2018-08-28 01:51:23','2020-11-20 13:34:04','1'),(5003,'二级菜单',NULL,'',5000,'icon-caidanguanli','',1,'0','0','2018-11-21 17:49:18','2018-11-21 17:53:25','0'),(5004,'二级菜单',NULL,'/index',5003,'icon-caidanguanli','/admin/dashboard/dashboard',1,'0','0','2018-11-21 17:53:51','2020-11-20 13:34:04','1'),(9999,'系统官网',NULL,'/about',-1,'icon-guanwangfangwen','/admin/dashboard/about',9,'0','0','2019-01-17 17:05:19','2020-11-20 13:40:40','0');
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_oauth_client_details`
--

DROP TABLE IF EXISTS `sys_oauth_client_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_oauth_client_details` (
  `client_id` varchar(32) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='终端信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_oauth_client_details`
--

LOCK TABLES `sys_oauth_client_details` WRITE;
/*!40000 ALTER TABLE `sys_oauth_client_details` DISABLE KEYS */;
INSERT INTO `sys_oauth_client_details` VALUES ('api',NULL,'api','server','password,refresh_token,authorization_code,client_credentials',NULL,NULL,NULL,NULL,NULL,'true'),('app',NULL,'app','server','password,refresh_token',NULL,NULL,NULL,NULL,NULL,'true'),('daemon',NULL,'daemon','server','password,refresh_token',NULL,NULL,NULL,NULL,NULL,'true'),('gen',NULL,'gen','server','password,refresh_token',NULL,NULL,NULL,NULL,NULL,'true'),('showbt','','showbt','server','password,refresh_token,authorization_code,client_credentials','http://localhost:4040/sso1/login,http://localhost:4041/sso1/login',NULL,NULL,NULL,NULL,'true'),('test',NULL,'test','server','password,refresh_token',NULL,NULL,NULL,NULL,NULL,'true');
/*!40000 ALTER TABLE `sys_oauth_client_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `role_code` varchar(64) COLLATE utf8mb4_bin NOT NULL,
  `role_desc` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `del_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '删除标识（0-正常,1-删除）',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `role_idx1_role_code` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='系统角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'管理员','ROLE_ADMIN','管理员','2017-10-29 15:45:51','2018-12-26 14:09:11','0'),(2,'ROLE_CQQ','ROLE_CQQ','ROLE_CQQ','2018-11-11 19:42:26','2018-12-26 14:09:07','0'),(3,'test','test','test','2019-05-20 14:45:01',NULL,'0');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_dept`
--

DROP TABLE IF EXISTS `sys_role_dept`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_dept` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(20) DEFAULT NULL COMMENT '角色ID',
  `dept_id` int(20) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色与部门对应关系';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_dept`
--

LOCK TABLES `sys_role_dept` WRITE;
/*!40000 ALTER TABLE `sys_role_dept` DISABLE KEYS */;
INSERT INTO `sys_role_dept` VALUES (1,1,8);
/*!40000 ALTER TABLE `sys_role_dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_menu`
--

DROP TABLE IF EXISTS `sys_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_menu`
--

LOCK TABLES `sys_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` VALUES (1,1000),(1,1100),(1,1101),(1,1102),(1,1103),(1,1200),(1,1201),(1,1202),(1,1203),(1,1300),(1,1301),(1,1302),(1,1303),(1,1304),(1,1400),(1,1401),(1,1402),(1,1403),(1,2000),(1,2100),(1,2101),(1,2200),(1,2201),(1,2202),(1,2203),(1,2300),(1,2400),(1,2401),(1,2402),(1,2403),(1,2500),(1,2600),(1,2601),(1,9999),(2,1000),(2,1100),(2,1101),(2,1102),(2,1103),(2,1200),(2,1201),(2,1202),(2,1203),(2,1300),(2,1301),(2,1302),(2,1303),(2,1304),(2,1400),(2,1401),(2,1402),(2,1403);
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `username` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '用户名',
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '随机盐',
  `phone` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '简介',
  `avatar` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
  `dept_id` int(11) DEFAULT NULL COMMENT '部门ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `lock_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '0-正常，9-锁定',
  `del_flag` char(1) COLLATE utf8mb4_bin DEFAULT '0' COMMENT '0-正常，1-删除',
  `wx_openid` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信openid',
  `qq_openid` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'QQ openid',
  PRIMARY KEY (`user_id`),
  KEY `user_wx_openid` (`wx_openid`),
  KEY `user_qq_openid` (`qq_openid`),
  KEY `user_idx1_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin ROW_FORMAT=DYNAMIC COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'admin','$2a$10$RpFJjxYiXdEsAGnWp/8fsOetMuOON96Ntk/Ym2M/RKRyU0GZseaDC',NULL,'17034642999','',1,'2018-04-20 07:15:18','2019-01-31 14:29:07','0','0','o_0FT0uyg_H1vVy2H0JpSwlVGhWQ',NULL),(2,'luolin','$2a$10$TW0BQojmHC/n/ITHflDf0uxADn4CL3HvQrj7wPe7iYE31WAwYfmJK',NULL,'',NULL,13,'2019-05-20 14:44:41','2019-05-20 14:44:49','0','0',NULL,NULL);
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user_role`
--

DROP TABLE IF EXISTS `sys_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user_role`
--

LOCK TABLES `sys_user_role` WRITE;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-22 21:25:57
