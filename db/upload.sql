ALTER TABLE `xh_base_cloud`.`sys_menu`
CHANGE COLUMN `menu_id` `menu_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID' ;


2020-11-25
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/upms/User' WHERE (`menu_id` = '1100');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/upms/Menu' WHERE (`menu_id` = '1200');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/upms/Role' WHERE (`menu_id` = '1300');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/upms/Depart' WHERE (`menu_id` = '1400');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/system/Log' WHERE (`menu_id` = '2100');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/system/Dict' WHERE (`menu_id` = '2200');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/system/Gen' WHERE (`menu_id` = '2300');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/system/Client' WHERE (`menu_id` = '2400');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/system/Token' WHERE (`menu_id` = '2600');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/dashboard/Dashboard' WHERE (`menu_id` = '5001');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/dashboard/Dashboard' WHERE (`menu_id` = '5002');
UPDATE `xh_base_cloud`.`sys_menu` SET `component` = '/admin/dashboard/Dashboard' WHERE (`menu_id` = '5004');



2020-11-27
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '' WHERE (`menu_id` = '1000');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/upms/user' WHERE (`menu_id` = '1100');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/upms/menu' WHERE (`menu_id` = '1200');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/upms/role' WHERE (`menu_id` = '1300');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/upms/dept' WHERE (`menu_id` = '1400');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '' WHERE (`menu_id` = '2000');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/log' WHERE (`menu_id` = '2100');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/dict' WHERE (`menu_id` = '2200');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/gen' WHERE (`menu_id` = '2300');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/client' WHERE (`menu_id` = '2400');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/token' WHERE (`menu_id` = '2600');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/crud' WHERE (`menu_id` = '5000');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/index' WHERE (`menu_id` = '5001');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/crud' WHERE (`menu_id` = '5002');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/admin/system/index' WHERE (`menu_id` = '5004');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '' WHERE (`menu_id` = '10000');
UPDATE `xh_base_cloud`.`sys_menu` SET `path` = '/test/test001' WHERE (`menu_id` = '10001');


2020-11-30
INSERT INTO `xh_base_cloud.sys_menu` (`menu_id`,`name`,`permission`,`path`,`parent_id`,`icon`,`component`,`sort`,`keep_alive`,`type`,`create_time`,`update_time`,`del_flag`) VALUES (10006,'任务管理',NULL,'/admin/task',10002,NULL,'/admin/gateway/TaskList',1,'0','0','2020-11-30 08:00:05','2020-11-30 08:07:45','0');
INSERT INTO `xh_base_cloud.sys_menu` (`menu_id`,`name`,`permission`,`path`,`parent_id`,`icon`,`component`,`sort`,`keep_alive`,`type`,`create_time`,`update_time`,`del_flag`) VALUES (10007,'策略管理',NULL,'/admin/strategy',10002,NULL,'/admin/gateway/StrategyList',1,'0','0','2020-11-30 08:03:02','2020-11-30 08:07:52','0');
INSERT INTO `xh_base_cloud.sys_menu` (`menu_id`,`name`,`permission`,`path`,`parent_id`,`icon`,`component`,`sort`,`keep_alive`,`type`,`create_time`,`update_time`,`del_flag`) VALUES (10008,'应用管理',NULL,'/admin/app',10002,NULL,'/admin/gateway/AppList',1,'0','0','2020-11-30 08:10:36',NULL,'0');



2020-12-09
INSERT INTO `xh_base_cloud.cpt_server` (`id`,`create_by`,`create_time`,`remark`,`status`,`update_by`,`update_time`,`label`,`real_path`,`server_name`,`m_id`,`fixed_params`,`mapping_fields`,`method`,`request_param_keys`,`g_id`,`custom_param`,`request_headers`,`request_key_mapping`,`server_type`,`formatter_class`,`v1_g_id`) VALUES ('000000007646ceac017646d02fcb0000',NULL,'2020-12-09 09:23:38',NULL,1,NULL,NULL,'/root','/app/root','获取一级应用','8afd8aed6f6930aa016fa7d0dfe70019',NULL,NULL,'GET',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

2021-02-20
insert into xh_base_cloud.cpt_server_rule(args, filter_or_rule_order, rule_id, server_id) value('{}', NULL, '8afd8aed6e6406ed016e8b9ef4050018', '8afd8aed6e6292bb016e63952611000e');